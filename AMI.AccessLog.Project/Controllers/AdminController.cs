﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMI.AccessLog.Project.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;

namespace AMI.AccessLog.Project.Controllers
{
    public class AdminController : Controller
    {
        AMI_ACCESSLOG_DBContext _db = new AMI_ACCESSLOG_DBContext();
        Common common = new Common();

        private IHostingEnvironment _env;
        public ISession session;

        public AdminController(IHostingEnvironment env)
        {
            _env = env;
        }

        #region ++ Role ++
        public IActionResult Role(Role role)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

            ViewBag.RoleList = _db.Role.OrderByDescending(r => r.RoleName).ToList();
			//ViewBag.RoleList = (from t1 in _db.Role orderby t1.CreatedDate descending
			//					select new Role
			//					{
			//						RoleId = t1.RoleId,
			//						RoleName = t1.RoleName,
			//						Description = t1.Description
			//					}
			//					).ToList();

			return View(role);
        }

		bool CheckRoleForm(Role model)
        {
            if (model.RoleName == null)
            {
                TempData["Message"] = "*Please fill all the required fields";
                return false;
            }
            return true;
        }

        public IActionResult SaveRole(Role role)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (role.RoleId == Guid.Empty)
                {
                    bool check = CheckRoleForm(role);
                    if (!check)
                    {
					ViewBag.RoleList = _db.Role.OrderByDescending(r => r.RoleName).ToList();
					return View("Role" , role);
                    }
                    else
                    {
                        role.RoleId = Guid.NewGuid();
						role.CreatedDate = DateTime.Now;
					_db.Entry(role).State = EntityState.Added;
                        _db.SaveChanges();
                        TempData["Message"] = "Save Successful.";
                        return RedirectToAction("Role");
                    }
                }
                else
                {
                    bool check = CheckRoleForm(role);
                    if (ModelState.IsValid && check == true)
                    {
                        _db.Update(role);
                        _db.SaveChanges();
                        TempData["Message"] = "Update Successful.";
                        return RedirectToAction("Role");
                    }
                }
			ViewBag.RoleList = _db.Role.OrderByDescending(r => r.RoleName).ToList();
			return View("Role", role);
		}

		public IActionResult DeleteRole(Guid roleId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var role = _db.Role.AsNoTracking().SingleOrDefault(m => m.RoleId == roleId);
            if (role == null)
            {
                return RedirectToAction(nameof(Role));
            }

            try
            {
                var userList = _db.SysUser.Where(u => u.Role == roleId).ToList();

				if (userList.Count == 0)
                {
                    _db.Role.Remove(role);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successful.";
                    return RedirectToAction("Role", new Role());
                }
                else
                {
                    TempData["Message"] = "This role has already used in process.";
                }
                ViewBag.RoleList = _db.Role.ToList();
                return RedirectToAction("Role", role);
            }
            catch (DbUpdateException)
            {
                return View("Role", new { id = roleId, saveChangesError = true });
            }
        }

        public IActionResult EditRole(Guid roleId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			Role role = _db.Role.Where(r => r.RoleId == roleId).FirstOrDefault();
			ViewBag.RoleList = _db.Role.OrderByDescending(r => r.RoleName).ToList();
			return View("Role", role);
		}

		public IActionResult ExportRoleList()
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var rolelist = _db.Role.OrderBy(r => r.RoleName).ToList();

            var columnHeader = new List<string>
            {
                "Name",
                "Description"
            };

            var fileContent = ExportExcelForRoleList(rolelist, columnHeader, "Role List");
            return File(fileContent, "application/ms-excel", "RoleList.xlsx");
        }

        public byte[] ExportExcelForRoleList(List<Role> roleList, List<string> columnHeader, string heading)
        {
			byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in roleList)
                {
                    worksheet.Cells["A" + j].Value = item.RoleName;
                    worksheet.Cells["B" + j].Value = item.Description;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #endregion

        public bool CheckLoginUser()
        {
            var userId = HttpContext.Session.GetString("userId");
            if (userId == null)
            {
                return false;
            }
            return true;

        }

		#region ++ Position ++
		public IActionResult Position(Position position)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.PositionList = _db.Position.OrderByDescending(p => p.Name).ToList();
			return View(position);
		}

		bool CheckPositionForm(Position model)
		{
			if (model.Name == null)
			{
				TempData["Message"] = "*Please fill all the required fields";
				return false;
			}
			return true;
		}
		public IActionResult SavePosition(Position position)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (position.Id == Guid.Empty)
			{
				bool check = CheckPositionForm(position);
				if (!check)
				{
					ViewBag.PositionList = _db.Position.OrderByDescending(p => p.Name).ToList();
					return View("Position",position);
				}
				else
				{
					position.Id = Guid.NewGuid();
					position.CreatedDate = DateTime.Now;
					_db.Entry(position).State = EntityState.Added;
					_db.SaveChanges();
					TempData["Message"] = "Save Successful.";
					return RedirectToAction("Position");
				}
			}
			else
			{
				bool check = CheckPositionForm(position);
				if (ModelState.IsValid && check == true)
				{
					_db.Update(position);
					_db.SaveChanges();
					TempData["Message"] = "Update Successful.";
					return RedirectToAction("Position");
				}
			}
			ViewBag.PositionList = _db.Position.OrderByDescending(p => p.Name).ToList();
			return View("Position", position);
		}

		public IActionResult EditPosition(Guid Id)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			Position position = _db.Position.Where(p => p.Id == Id).FirstOrDefault();
			ViewBag.PositionList = _db.Position.OrderByDescending(p => p.Name).ToList();
			return View("Position", position);
		}

		public IActionResult DeletePosition(Guid Id)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var position = _db.Position.AsNoTracking().SingleOrDefault(m => m.Id == Id);
			if (position == null)
			{
				ViewBag.PositionList = _db.Position.OrderByDescending(p => p.Name).ToList();
				return View("Position", position);
			}

			try
			{
				var userList = _db.SysUser.Where(u => u.Position == Id).ToList();
				if (userList.Count == 0)
				{
					_db.Position.Remove(position);
					_db.SaveChanges();
					TempData["Message"] = "Delete Successful.";
					return RedirectToAction("Position", new Position());
				}
				else
				{
					TempData["Message"] = "This position has already used in process.";
				}
				ViewBag.PositionList = _db.Position.OrderByDescending(p => p.Name).ToList();
				return View("Position", position);
			}
			catch (DbUpdateException)
			{
				return View("position", new { id = Id, saveChangesError = true });
			}
		}


		public IActionResult ExportPositionList()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var positionList = _db.Position.OrderBy(p => p.Name).ToList();

			var columnHeader = new List<string>
			{
				"Name",
				"Description"
			};

			var fileContent = ExportExcelForPositionList(positionList, columnHeader, "Position List");
			return File(fileContent, "application/ms-excel", "PositionList.xlsx");
		}


		public byte[] ExportExcelForPositionList(List<Position> positionList, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;
				using (var cells = worksheet.Cells[1, 1, 1, 8])
				{
					cells.Style.Font.Bold = true;
				}

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[1, i + 1].Value = columnHeader[i];
				}
				var j = 2;
				var count = 1;
				foreach (var item in positionList)
				{
					worksheet.Cells["A" + j].Value = item.Name;
					worksheet.Cells["B" + j].Value = item.Description;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}

		#endregion++ Position ++

		#region ++ UserList ++
		public IActionResult ResignUser(SysUser user)
		{
			ViewBag.UserList = (from u in _db.SysUser
								join r in _db.Role on u.Role equals r.RoleId
								join t in _db.Team on u.TeamId equals t.Id into b from sub in b.DefaultIfEmpty()
								join p in _db.Position on u.Position equals p.Id
								where u.IsActive == false
								orderby u.Name descending
								select new UserInfo
								{
									Id = u.Id,
									Name = u.Name,
									Email = u.Email,
									Team = sub.Name,
									Role = r.RoleName,
									Position = p.Name,
									IsActive = u.IsActive
								}).ToList();
			return View(user);
		}

		void GetViewBagDataForUser()
		{
			ViewBag.RoleList = new SelectList(_db.Role.OrderBy(r => r.RoleName).ToList(), "RoleId", "RoleName");
			ViewBag.PositionList = new SelectList(_db.Position.OrderBy(p => p.Name).ToList(), "Id", "Name");
			ViewBag.UserList = (from u in _db.SysUser
								join r in _db.Role on u.Role equals r.RoleId
								join t in _db.Team on u.TeamId equals t.Id into b
								from sub in b.DefaultIfEmpty()
								join p in _db.Position on u.Position equals p.Id
								where u.IsActive == true
								orderby u.Name descending
								select new UserInfo
								{
									Id = u.Id,
									Name = u.Name,
									Email = u.Email,
									Team = sub.Name,
									Role = r.RoleName,
									Position = p.Name,
									IsActive = u.IsActive
								}).ToList();
			ViewBag.TeamList = new SelectList(_db.Team.OrderBy(t => t.Name).ToList(), "Id", "Name");
		}
		public IActionResult UserList(SysUser user)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			GetViewBagDataForUser();
			return View(user);
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        bool CheckSaveUserForm(SysUser model)
        {
            bool check = IsValidEmail(model.Email);
            if (model.Role == Guid.Empty || model.TeamId == null || model.Name == null || model.Email == null)
            {
                TempData["Message"] = "*Please fill all the required fields";
                return false;
            }
            if (!check)
            {
                TempData["Message"] = "Invalid Email Address";
                return false;
            }
            return true;
        }
        public IActionResult SaveUser(SysUser model)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");
			try
			{
                if (model.Id == Guid.Empty)
                {
                    //bool check = CheckSaveUserForm(model);
                    //if(!check)
                    //{
						//GetViewBagDataForUser();
						//return View("UserList", model);
                    //}
                    //else
                    //{
                        model.Id = Guid.NewGuid();
                        model.Email = model.Email.ToUpper();
                        model.CreatedDate = DateTime.Now;
                        model.FirstTimeLogin = true;
                        model.PasswordSalt = common.GenerateSalt(8);
                        model.Password = common.EncryptPassword("12345678", model.PasswordSalt);
                        model.IsActive = true;

                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();
                        //ViewBag.Message = "Save Successful.";
                        TempData["Message"] = "Save Successful.";
                        return RedirectToAction("UserList");
                   //}
                }
                else
                {
                    bool check = CheckSaveUserForm(model);

                    if (ModelState.IsValid && check ==  true)
                    {
                        _db.SysUser.Update(model);
                        _db.SaveChanges();
                        TempData["Message"] = "Update Successful.";
                        return RedirectToAction("UserList");
                    }
                }
            }
            catch (DbUpdateException ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("UserList");
            }
			GetViewBagDataForUser();
			return View("UserList", model);
		}

        public IActionResult ExportUserList()
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var userInfos = (from u in _db.SysUser
							 join r in _db.Role on u.Role equals r.RoleId
							 join p in _db.Position on u.Position equals p.Id
							 orderby u.Name
							 select new UserInfo
							 {
								 Id = u.Id,
								 Name = u.Name,
								 Email = u.Email,
								 Role = r.RoleName,
								 Position = p.Name,
								 IsActive = u.IsActive
							 }).ToList();

			var columnHeader = new List<string>
            {
                "Name",
                "Email",
                "Role",
                "Position",
				"Active Status",
			};

            var fileContent = ExportExcelForUserList(userInfos, columnHeader, "User List");
            return File(fileContent, "application/ms-excel", "UserList.xlsx");
        }

        public byte[] ExportExcelForUserList(List<UserInfo> userList, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
				string status;
				foreach (var item in userList)
                {
					if (item.IsActive)
					{
						status = "Active";
					}
					else
					{
						status = "Not Active";
					}
					worksheet.Cells["A" + j].Value = item.Name;
                    worksheet.Cells["B" + j].Value = item.Email;
                    worksheet.Cells["C" + j].Value = item.Role;
                    worksheet.Cells["D" + j].Value = item.Position;
                    worksheet.Cells["E" + j].Value = status;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult EditUser(Guid UserId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var sysUser = _db.SysUser.Where(u => u.Id == UserId).FirstOrDefault();
			GetViewBagDataForUser();
			return View("UserList", sysUser);
        }

        public IActionResult DeleteUser(Guid Id)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (Id != Guid.Empty)
            {
                SysUser model = _db.SysUser.Where(u => u.Id == Id).FirstOrDefault();
                //var registerList = _db.Register.Where(u => u.TechnicianName == model.Name).ToList();
                var registerList = _db.Register.Where(u => u.SysUserId == model.Id).ToList();
				var RequirementItemsList = _db.RequirementItems.Where(r => r.CheckTechnicianId == model.Id || r.ImplementTechnicianId == model.Id).ToList();
				if (registerList.Count == 0 && RequirementItemsList.Count == 0)
                {
                    SysUser sysUser = _db.SysUser.Where(u => u.Id == model.Id).FirstOrDefault();
                    _db.SysUser.Remove(sysUser);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successful.";
                }
                else
                {
                    TempData["Message"] = "This User has already used in process.";
                }
            }
            return RedirectToAction("UserList");
        }
		#endregion

	}
}