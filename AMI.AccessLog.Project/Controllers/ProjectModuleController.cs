﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AMI.AccessLog.Project.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;

namespace AMI.AccessLog.Project.Controllers
{
    public class ProjectModuleController : Controller
    {
		AMI_ACCESSLOG_DBContext _db = new AMI_ACCESSLOG_DBContext();
		Common common = new Common();
		public ISession session;

		private IHostingEnvironment _env;

		public ProjectModuleController(IHostingEnvironment env)
		{
			_env = env;
		}

		#region ++ Project ++

		bool CheckProjectForm(ProjectData model)
		{
			if (model.Name == null || model.TeamId == null)
			{
				TempData["Message"] = "*Please fill all the required fields";
				return false;
			}
			return true;
		}

		public IActionResult ExportFinishedProject()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var project_list = (from t1 in _db.ProjectData
								join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id into gj
								from subStatus in gj.DefaultIfEmpty()
								orderby t1.CreatedDate descending
								where t1.ProjectStatusId == Guid.Parse("62E18AAF-A16B-406B-84E8-BD99CCF85613")
								select new ProjectDataViewModel
								{
									Id = t1.Id,
									Name = t1.Name,
									StatusName = subStatus.Name ?? String.Empty,
									Description = t1.Description,
									CreatedDate = t1.CreatedDate
								}
				   ).ToList();

			var columnHeader = new List<string>
			{
				"Created Date",
				"Name",
				"Project Status",
				"Description",
			};

			var fileContent = ExportExcelForProjectList(project_list, columnHeader, "Finished Project List");
			return File(fileContent, "application/ms-excel", "Finished Project List.xlsx");
		}

        public IActionResult ExportTotalFinishedProject()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var project_list = (from t1 in _db.ProjectData
                                join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id into gj
                                from subStatus in gj.DefaultIfEmpty()
                                orderby t1.CreatedDate descending
                                select new ProjectDataViewModel
                                {
                                    Id = t1.Id,
                                    Name = t1.Name,
                                    StatusName = subStatus.Name ?? String.Empty,
                                    Description = t1.Description,
                                    CreatedDate = t1.CreatedDate
                                }
                   ).ToList();

            var columnHeader = new List<string>
            {
                "Created Date",
                "Name",
                "Project Status",
                "Description",
            };

            var fileContent = ExportExcelForProjectList(project_list, columnHeader, "Total Project List");
            return File(fileContent, "application/ms-excel", "Total Project List.xlsx");
        }

        public IActionResult ExportDefinedFinishedProject()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

         
            var project_list = (from t1 in _db.ProjectData
                                   join t in _db.Team on t1.TeamId equals t.Id into b
                                   from sub in b.DefaultIfEmpty()
                                   join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id into gj
                                   from subStatus in gj.DefaultIfEmpty()
                                   where (t1.ProjectStatusId == null)
                                   select new ProjectDataViewModel
                                   {
                                       Id = t1.Id,
                                       Name = t1.Name,
                                       Team = sub.Name,
                                       StatusName = subStatus.Name ?? String.Empty,
                                       Description = t1.Description,
                                       CreatedDate = t1.CreatedDate
                                   }).ToList();

            var columnHeader = new List<string>
            {
                "Created Date",
                "Name",
                "Project Status",
                "Description",
            };

            var fileContent = ExportExcelForProjectList(project_list, columnHeader, "Underfined Project List");
            return File(fileContent, "application/ms-excel", "Underfined Project List.xlsx");
        }

        public IActionResult ExportProjectList()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var project_list = (from t1 in _db.ProjectData
							   join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id into gj
							   from subStatus in gj.DefaultIfEmpty()
							   orderby t1.CreatedDate descending
								where t1.ProjectStatusId != Guid.Parse("62E18AAF-A16B-406B-84E8-BD99CCF85613")
								select new ProjectDataViewModel
							   {
								   Id = t1.Id,
								   Name = t1.Name,
								   StatusName = subStatus.Name ?? String.Empty,
								   Description = t1.Description,
								   CreatedDate = t1.CreatedDate
							   }
				   ).ToList();

			var columnHeader = new List<string>
			{
				"Created Date",
				"Name",
				"Project Status",
				"Description",
			};

			var fileContent = ExportExcelForProjectList(project_list, columnHeader, "Ongoing Project List");
			return File(fileContent, "application/ms-excel", "Ongoing Project List.xlsx");
		}

		public byte[] ExportExcelForProjectList(List<ProjectDataViewModel> project_list, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;
				using (var cells = worksheet.Cells[1, 1, 1, 8])
				{
					cells.Style.Font.Bold = true;
				}

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[1, i + 1].Value = columnHeader[i];
				}
				var j = 2;
				var count = 1;
				foreach (var item in project_list)
				{
					worksheet.Cells["A" + j].Value = item.CreatedDate.Value.ToString("dd/MMM/yyyy");
					worksheet.Cells["B" + j].Value = item.Name;
					worksheet.Cells["C" + j].Value = item.StatusName;
					worksheet.Cells["D" + j].Value = item.Description;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}

		void GetViewBagDataForProject()
		{
			//62e18aaf - a16b - 406b - 84e8 - bd99ccf85613
			ViewBag.ProjectList = (from t1 in _db.ProjectData
								   join t in _db.Team on t1.TeamId equals t.Id into b
								   from sub in b.DefaultIfEmpty()
								   join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id into gj
								   from subStatus in gj.DefaultIfEmpty()
								   orderby t1.CreatedDate descending
								   where t1.ProjectStatusId != Guid.Parse("62E18AAF-A16B-406B-84E8-BD99CCF85613")
								   select new ProjectDataViewModel
								   {
									   Id = t1.Id,
									   Name = t1.Name,
									   Team = sub.Name,
									   StatusName = subStatus.Name ?? String.Empty,
									   Description = t1.Description,
									   CreatedDate = t1.CreatedDate
								   }).ToList();
			ViewBag.TeamList = new SelectList(_db.Team.OrderBy(t => t.Name).ToList(), "Id", "Name");
			ViewBag.ProjectStatusList = new SelectList(_db.ProjectStatus.OrderBy(P => P.Name).ToList(), "Id", "Name");
		}

		public IActionResult Project(ProjectData project)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			GetViewBagDataForProject();
			return View(project);
		}

		public IActionResult FinishedProject(ProjectData project)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.ProjectList = (from t1 in _db.ProjectData
								   join t in _db.Team on t1.TeamId equals t.Id into b
								   from sub in b.DefaultIfEmpty()
								   join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id into gj
								   from subStatus in gj.DefaultIfEmpty()
								   where t1.ProjectStatusId == Guid.Parse("62E18AAF-A16B-406B-84E8-BD99CCF85613")
                                   orderby t1.CreatedDate descending
								   select new ProjectDataViewModel
								   {
									   Id = t1.Id,
									   Name = t1.Name,
									   Team = sub.Name,
									   StatusName = subStatus.Name ?? String.Empty,
									   Description = t1.Description,
									   CreatedDate = t1.CreatedDate
								   }).ToList();
			ViewBag.TeamList = new SelectList(_db.Team.OrderBy(t => t.Name).ToList(), "Id", "Name");
			ViewBag.ProjectStatusList = new SelectList(_db.ProjectStatus.OrderBy(P => P.Name).ToList(), "Id", "Name");
			return View(project);
		}

        public IActionResult TotalProject(ProjectData project)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ViewBag.ProjectList = (from t1 in _db.ProjectData
                                   join t in _db.Team on t1.TeamId equals t.Id into b
                                   from sub in b.DefaultIfEmpty()
                                   join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id into gj
                                   from subStatus in gj.DefaultIfEmpty()
                                   orderby t1.CreatedDate descending
                                   select new ProjectDataViewModel
                                   {
                                       Id = t1.Id,
                                       Name = t1.Name,
                                       Team = sub.Name,
                                       StatusName = subStatus.Name ?? String.Empty,
                                       Description = t1.Description,
                                       CreatedDate = t1.CreatedDate
                                   }).ToList();
            ViewBag.TeamList = new SelectList(_db.Team.OrderBy(t => t.Name).ToList(), "Id", "Name");
            ViewBag.ProjectStatusList = new SelectList(_db.ProjectStatus.OrderBy(P => P.Name).ToList(), "Id", "Name");
            return View(project);
        }

        public IActionResult UndefinedProject(ProjectData project)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            ViewBag.ProjectList = (from t1 in _db.ProjectData
                                   join t in _db.Team on t1.TeamId equals t.Id into b
                                   from sub in b.DefaultIfEmpty()
                                   join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id into gj
                                   from subStatus in gj.DefaultIfEmpty()
                                   where (t1.ProjectStatusId == null)
                                   orderby t1.CreatedDate descending
                                   select new ProjectDataViewModel
                                   {
                                       Id = t1.Id,
                                       Name = t1.Name,
                                       Team = sub.Name,
                                       StatusName = subStatus.Name ?? String.Empty,
                                       Description = t1.Description,
                                       CreatedDate = t1.CreatedDate
                                   }).ToList();
            ViewBag.TeamList = new SelectList(_db.Team.OrderBy(t => t.Name).ToList(), "Id", "Name");
            ViewBag.ProjectStatusList = new SelectList(_db.ProjectStatus.OrderBy(P => P.Name).ToList(), "Id", "Name");
            return View(project);
        }

        public IActionResult SaveProject(ProjectData project)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (project.Id == Guid.Empty)
			{
				bool check = CheckProjectForm(project);
				if (!check)
				{
					GetViewBagDataForProject();
					return View("Project", project);
				}
				else
				{
					project.Id = Guid.NewGuid();
					project.CreatedDate = DateTime.Now;
					_db.Entry(project).State = EntityState.Added;
					_db.SaveChanges();
					TempData["Message"] = "Save Successful.";
					return RedirectToAction("Project");
				}
			}
			else
			{
				bool check = CheckProjectForm(project);
				if (ModelState.IsValid && check == true)
				{
					_db.Update(project);
					_db.SaveChanges();
					TempData["Message"] = "Update Successful.";
					return RedirectToAction("Project");
				}
			}
			GetViewBagDataForProject();
			return View("Project", project);
		}
		public IActionResult EditProject(Guid projectId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ProjectData project = _db.ProjectData.Where(r => r.Id == projectId).FirstOrDefault();
			GetViewBagDataForProject();
			return View("Project", project);
		}

		public IActionResult DeleteProject(Guid projectId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var project = _db.ProjectData.AsNoTracking().SingleOrDefault(m => m.Id == projectId);
			if (project == null)
			{
				GetViewBagDataForProject();
				return View("Project");
			}
			try
			{
				var projectRequirementItemsList = _db.RequirementItems.Where(u => u.ProjectId == projectId).ToList();
				if (projectRequirementItemsList.Count == 0)
				{
					_db.ProjectData.Remove(project);
					_db.SaveChanges();
					TempData["Message"] = "Delete Successful.";
					return RedirectToAction("Project", new ProjectData());
				}
				else
				{
					TempData["Message"] = "This project has already used in process.";
				}
				GetViewBagDataForProject();
				return View("Project", project);
			}
			catch (DbUpdateException)
			{
				return View("Project", new { id = projectId, saveChangesError = true });
			}
		}


		#endregion ++ Project ++

		public IActionResult ItemsListForStaff(RequirementItems items)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			GetViewBagDataForStaff(Guid.Parse(HttpContext.Session.GetString("userId")));
			return View(items);
		}

		public IActionResult EditItemForStaff(Guid itemId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			RequirementItems items = _db.RequirementItems.Where(r => r.Id == itemId).FirstOrDefault();
			GetViewBagDataForStaff(Guid.Parse(HttpContext.Session.GetString("userId")));
			return View("ItemsListForStaff", items);
		}

		public IActionResult SaveItemsForStaff(RequirementItems items)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (items.Id == Guid.Empty)
			{
				if (!CheckItems(items))
				{
					GetViewBagDataForStaff(Guid.Parse(HttpContext.Session.GetString("userId")));
					return View("ItemsListForStaff", items);
				}
				else
				{
					items.Id = Guid.NewGuid();
					//items.Date =  items.CreatedDate;
					items.CreatedDate = DateTime.Now;
					_db.Entry(items).State = EntityState.Added;
					_db.SaveChanges();
					TempData["Message"] = "Save Successful.";
					return RedirectToAction("ItemsListForStaff");
				}
			}
			else
			{
				bool check = CheckItems(items);
				if (ModelState.IsValid && check == true)
				{
					_db.Update(items);
					_db.SaveChanges();
					TempData["Message"] = "Update Successful.";
					return RedirectToAction("ItemsListForStaff");
				}
			}
			return RedirectToAction("ItemsListForStaff", items);
		}


		#region ++ Status ++
		public IActionResult Status(Status status)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.StatusList = _db.Status.OrderBy(s => s.Name).ToList();
			return View(status);
		}

		public IActionResult ExportStatusList()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var status_list = _db.Status.OrderBy(s => s.Name).ToList();

			var columnHeader = new List<string>
			{
				"Name",
				"Description",
			};

			var fileContent = ExportExcelForStatusList(status_list, columnHeader, "Status List");
			return File(fileContent, "application/ms-excel", "StatusList.xlsx");
		}


		public byte[] ExportExcelForStatusList(List<Status> status_list, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;
				using (var cells = worksheet.Cells[1, 1, 1, 8])
				{
					cells.Style.Font.Bold = true;
				}

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[1, i + 1].Value = columnHeader[i];
				}
				var j = 2;
				var count = 1;
				foreach (var item in status_list)
				{
					worksheet.Cells["A" + j].Value = item.Name;
					worksheet.Cells["B" + j].Value = item.Description;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}

		public IActionResult SaveStatus(Status status)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (status.Id == Guid.Empty)
			{
				bool check = CheckForm(status.Name);
				if (!check)
				{
					ViewBag.StatusList = _db.Status.OrderBy(s => s.Name).ToList();
					return View("Status", status);
				}
				else
				{
					status.Id = Guid.NewGuid();
					status.CreatedDate = DateTime.Now;
					_db.Entry(status).State = EntityState.Added;
					_db.SaveChanges();
					TempData["Message"] = "Save Successful.";
					return RedirectToAction("Status");
				}
			}
			else
			{
				bool check = CheckForm(status.Name);
				if (ModelState.IsValid && check == true)
				{
					_db.Update(status);
					_db.SaveChanges();
					TempData["Message"] = "Update Successful.";
					return RedirectToAction("Status");
				}
			}
			ViewBag.StatusList = _db.Status.OrderBy(s => s.Name).ToList();
			return View("Status", status);
		}
		public IActionResult EditStatus(Guid statusId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			Status status = _db.Status.Where(r => r.Id == statusId).FirstOrDefault();
			ViewBag.StatusList = _db.Status.OrderBy(s => s.Name).ToList();
			return View("Status", status);
		}

		public IActionResult DeleteStatus(Guid statusId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var status = _db.Status.AsNoTracking().SingleOrDefault(m => m.Id == statusId);
			if (status == null)
			{
				ViewBag.StatusList = _db.Status.OrderBy(s => s.Name).ToList();
				return View("Status");
			}
			try
			{
				var projectRequirementItemsList = _db.RequirementItems.Where(u => u.StatusId == statusId).ToList();
				if (projectRequirementItemsList.Count == 0)
				{
					_db.Status.Remove(status);
					_db.SaveChanges();
					TempData["Message"] = "Delete Successful.";
					ViewBag.StatusList = _db.Status.OrderBy(s => s.Name).ToList();
					return View("Status");
				}
				else
				{
					TempData["Message"] = "This status has already used in process.";
				}
				ViewBag.StatusList = _db.Status.OrderBy(s => s.Name).ToList();
				return View("Status", status);
			}
			catch (DbUpdateException)
			{
				return View("Status", new { id = statusId, saveChangesError = true });
			}
		}
		#endregion ++ Status ++


		#region ++ Project Requirement Items ++
		void GetViewBagDataForRequirementItems()
		{
			ProjectStatus status = _db.ProjectStatus.Where(s => s.Name == "100%").FirstOrDefault();
			ViewBag.ProjectList = new SelectList(_db.ProjectData.Where(p => p.ProjectStatusId != status.Id).OrderBy(s => s.Name).ToList(), "Id", "Name");
			ViewBag.ItemsList = (from t1 in _db.RequirementItems
								 join t2 in _db.ProjectData on t1.ProjectId equals t2.Id
								 join t3 in _db.SysUser on t1.CheckTechnicianId equals t3.Id
								 join t4 in _db.SysUser on t1.ImplementTechnicianId equals t4.Id
								 join t5 in _db.Status on t1.StatusId equals t5.Id into gj
								 from subStatus in gj.DefaultIfEmpty()
								 orderby t1.Date descending
								 select new RequirementItemsViewModel
								 {
									 Id = t1.Id,
									 Description = t1.Description,
									 Date = t1.Date,
									 StartDate = t1.StartDate,
									 EndDate = t1.EndDate,
									 Remark = t1.Remark,
									 ProjectName = t2.Name,
									 StatusName = subStatus.Name ?? String.Empty,
									 ImplementTechnicianName = t4.Name,
									 CheckTechnicianName = t3.Name,
								 }).ToList();
			ViewBag.UserList = new SelectList(_db.SysUser.Where( u => u.IsActive == true).OrderBy(s => s.Name).ToList(), "Id", "Name");
			ViewBag.StatusList = new SelectList(_db.Status.OrderBy(s => s.Name).ToList(), "Id", "Name");
		}

		void GetViewBagDataForStaff(Guid UserId)
		{
			ProjectStatus status = _db.ProjectStatus.Where(s => s.Name == "100%").FirstOrDefault();
			ViewBag.ProjectList = new SelectList(_db.ProjectData.Where(p => p.ProjectStatusId != status.Id).OrderBy(s => s.Name).ToList(), "Id", "Name");
			ViewBag.ItemsList = (from t1 in _db.RequirementItems
								 join t2 in _db.ProjectData on t1.ProjectId equals t2.Id
								 join t3 in _db.SysUser on t1.CheckTechnicianId equals t3.Id
								 join t4 in _db.SysUser on t1.ImplementTechnicianId equals t4.Id
								 join t5 in _db.Status on t1.StatusId equals t5.Id into gj
								 from subStatus in gj.DefaultIfEmpty()
								 where t1.ImplementTechnicianId == UserId
								 orderby t1.Date descending
								 select new RequirementItemsViewModel
								 {
									 Id = t1.Id,
									 Description = t1.Description,
									 Date = t1.Date,
									 StartDate = t1.StartDate,
									 EndDate = t1.EndDate,
									 Remark = t1.Remark,
									 ProjectName = t2.Name,
									 StatusName = subStatus.Name ?? String.Empty,
									 ImplementTechnicianName = t4.Name,
									 CheckTechnicianName = t3.Name,
								 }).ToList();
			ViewBag.UserList = new SelectList(_db.SysUser.Where(u => u.IsActive == true).OrderBy(s => s.Name).ToList(), "Id", "Name");
			ViewBag.StatusList = new SelectList(_db.Status.OrderBy(s => s.Name).ToList(), "Id", "Name");
		}

		public IActionResult ProjectRequirementItems(RequirementItems items)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			GetViewBagDataForRequirementItems();
			return View(items);
		}

		public IActionResult ExportItemsList()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var ItemsList = (from t1 in _db.RequirementItems
								join t2 in _db.ProjectData on t1.ProjectId equals t2.Id
								join t3 in _db.SysUser on t1.CheckTechnicianId equals t3.Id
								join t4 in _db.SysUser on t1.ImplementTechnicianId equals t4.Id
								join t5 in _db.Status on t1.StatusId equals t5.Id into gj
								from subStatus in gj.DefaultIfEmpty()
								orderby t1.Date descending
								select new RequirementItemsViewModel
								{
									Id = t1.Id,
									Description = t1.Description,
									Date = t1.Date,
									StartDate = t1.StartDate,
									EndDate = t1.EndDate,
									Remark = t1.Remark,
									ProjectName = t2.Name,
									StatusName = subStatus.Name ?? String.Empty,
									ImplementTechnicianName = t4.Name,
									CheckTechnicianName = t3.Name,
								}).ToList();

			var columnHeader = new List<string>
			{
				"Date",
				"Requirement Item",
				"Project Name",
				"Start Date",
				"End Date",
				"Assign / Implement Technician",
				"Assign / Check Technician",
				"Status",
				"Remark",
			};

			var fileContent = ExportExcelForItemsList(ItemsList, columnHeader, "Project Requirement Items");
			return File(fileContent, "application/ms-excel", "ProjectRequirementItems.xlsx");
		}


		public byte[] ExportExcelForItemsList(List<RequirementItemsViewModel> ItemsList, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;
				using (var cells = worksheet.Cells[1, 1, 1, 8])
				{
					cells.Style.Font.Bold = true;
				}

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[1, i + 1].Value = columnHeader[i];
				}
				var j = 2;
				var count = 1;
				
				foreach (var item in ItemsList)
				{
					//DateTime? startDate,endDate;
					if (item.StartDate != null)
					{
						worksheet.Cells["D" + j].Value = item.StartDate.Value.ToString("dd/MMM/yyyy");
					}
					else
					{
						worksheet.Cells["D" + j].Value = null;
					}

					if (item.EndDate != null)
					{
						worksheet.Cells["E" + j].Value = item.EndDate.Value.ToString("dd/MMM/yyyy");
					}
					else
					{
						worksheet.Cells["E" + j].Value = null;
					}

					worksheet.Cells["A" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.Date);
					worksheet.Cells["B" + j].Value = item.Description;
					worksheet.Cells["C" + j].Value = item.ProjectName;
					//worksheet.Cells["D" + j].Value = item.StartDate.Value.ToString("dd/MMM/yyyy hh:mm:ss tt");
					//worksheet.Cells["D" + j].Value = startDate.Value.ToString("dd/MMM/yyyy hh:mm:ss tt");
					////worksheet.Cells["E" + j].Value = item.EndDate.Value.ToString("dd/MMM/yyyy hh:mm:ss tt");
					//worksheet.Cells["E" + j].Value = endDate.Value.ToString("dd/MMM/yyyy hh:mm:ss tt");
					worksheet.Cells["F" + j].Value = item.ImplementTechnicianName;
					worksheet.Cells["G" + j].Value = item.CheckTechnicianName;
					worksheet.Cells["H" + j].Value = item.StatusName;
					worksheet.Cells["I" + j].Value = item.Remark;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}

		public IActionResult ExportItemsListForStaff(Guid UserId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var ItemsList = (from t1 in _db.RequirementItems
							 join t2 in _db.ProjectData on t1.ProjectId equals t2.Id
							 join t3 in _db.SysUser on t1.CheckTechnicianId equals t3.Id
							 join t4 in _db.SysUser on t1.ImplementTechnicianId equals t4.Id
							 join t5 in _db.Status on t1.StatusId equals t5.Id into gj
							 from subStatus in gj.DefaultIfEmpty()
							 where t1.ImplementTechnicianId == UserId
							 orderby t1.Date
							 select new RequirementItemsViewModel
							 {
								 Id = t1.Id,
								 Description = t1.Description,
								 Date = t1.Date,
								 StartDate = t1.StartDate,
								 EndDate = t1.EndDate,
								 Remark = t1.Remark,
								 ProjectName = t2.Name,
								 StatusName = subStatus.Name ?? String.Empty,
								 ImplementTechnicianName = t4.Name,
								 CheckTechnicianName = t3.Name,
							 }).ToList();

			var columnHeader = new List<string>
			{
				"Date",
				"Requirement Item",
				"Project Name",
				"Start Date",
				"End Date",
				"Assign / Implement Technician",
				"Assign / Check Technician",
				"Status",
				"Remark",
			};

			var fileContent = ExportExcelItemsForStaff(ItemsList, columnHeader, "Project Requirement Items List");
			return File(fileContent, "application/ms-excel", "ProjectRequirementItemsList.xlsx");
		}

		public byte[] ExportExcelItemsForStaff(List<RequirementItemsViewModel> ItemsList, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;
				using (var cells = worksheet.Cells[1, 1, 1, 8])
				{
					cells.Style.Font.Bold = true;
				}

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[1, i + 1].Value = columnHeader[i];
				}
				var j = 2;
				var count = 1;

				foreach (var item in ItemsList)
				{
					//DateTime? startDate,endDate;
					if (item.StartDate != null)
					{
						worksheet.Cells["D" + j].Value = item.StartDate.Value.ToString("dd/MMM/yyyy");
					}
					else
					{
						worksheet.Cells["D" + j].Value = null;
					}

					if (item.EndDate != null)
					{
						worksheet.Cells["E" + j].Value = item.EndDate.Value.ToString("dd/MMM/yyyy");
					}
					else
					{
						worksheet.Cells["E" + j].Value = null;
					}

					worksheet.Cells["A" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.Date);
					worksheet.Cells["B" + j].Value = item.Description;
					worksheet.Cells["C" + j].Value = item.ProjectName;
					worksheet.Cells["F" + j].Value = item.ImplementTechnicianName;
					worksheet.Cells["G" + j].Value = item.CheckTechnicianName;
					worksheet.Cells["H" + j].Value = item.StatusName;
					worksheet.Cells["I" + j].Value = item.Remark;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}

		public IActionResult SaveItems(RequirementItems items)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (items.Id == Guid.Empty)
			{
				bool check = CheckItems(items);
				if (!check)
				{
					GetViewBagDataForRequirementItems();
					return View("ProjectRequirementItems", items);
				}
				else
				{
					items.Id = Guid.NewGuid();
					//items.Date =  items.CreatedDate;
					items.CreatedDate = DateTime.Now;
					_db.Entry(items).State = EntityState.Added;
					_db.SaveChanges();
					TempData["Message"] = "Save Successful.";
					return RedirectToAction("ProjectRequirementItems");
				}
			}
			else
			{
				bool check = CheckItems(items);
				if (ModelState.IsValid && check == true)
					{
					_db.Update(items);
					_db.SaveChanges();
					TempData["Message"] = "Update Successful.";
					return RedirectToAction("ProjectRequirementItems");
				}
			}
			GetViewBagDataForRequirementItems();
			return View("ProjectRequirementItems", items);
		}
		public IActionResult EditItem(Guid itemId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			RequirementItems items = _db.RequirementItems.Where(r => r.Id == itemId).FirstOrDefault();
			GetViewBagDataForRequirementItems();
			return View("ProjectRequirementItems", items);
		}

		public IActionResult DeleteItem(Guid itemId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var item = _db.RequirementItems.AsNoTracking().SingleOrDefault(m => m.Id == itemId);
			if (item == null)
			{
				GetViewBagDataForRequirementItems();
				return View("ProjectRequirementItems", item);
			}
			_db.RequirementItems.Remove(item);
			_db.SaveChanges();
			TempData["Message"] = "Delete Successful.";
			return RedirectToAction("ItemsListForStaff");
		}

		bool CheckItems(RequirementItems model)
		{
			if (model.Date == null || model.Description == null || model.ProjectId == Guid.Empty || model.ImplementTechnicianId == Guid.Empty || model.CheckTechnicianId == Guid.Empty)
			{
				TempData["Message"] = "*Please fill all the required fields";
				return false;
			}
			return true;
		}


		//public IActionResult DeleteStatus(Guid statusId)
		//{
		//	bool result = CheckLoginUser();
		//	if (!result)
		//	{
		//		return RedirectToAction("Login", "Account");
		//	}
		//	var status = _db.Status.AsNoTracking().SingleOrDefault(m => m.Id == statusId);
		//	if (status == null)
		//	{
		//		return RedirectToAction(nameof(Status));
		//	}
		//	_db.Status.Remove(status);
		//	_db.SaveChanges();
		//	TempData["Message"] = "Delete Successful.";
		//	return RedirectToAction("Status", new Status());
		//	try
		//	{
		//		var projectList = _db.ProjectData.Where(u => u.Id == projectId).ToList();
		//		if (projectList.Count == 0)
		//		{
		//			_db.ProjectData.Remove(project);
		//			_db.SaveChanges();
		//			TempData["Message"] = "Delete Successful.";
		//			return RedirectToAction("Project", new ProjectData());
		//		}
		//		else
		//		{
		//			TempData["Message"] = "This project has already used in process.";
		//		}
		//		ViewBag.ProjectList = _db.ProjectData.OrderBy(P => P.Name).ToList();
		//		return RedirectToAction("Project", project);
		//	}
		//	catch (DbUpdateException)
		//	{
		//		return View("Project", new { id = projectId, saveChangesError = true });
		//	}
		//}
		#endregion ++ Project Requirement Items ++


		#region ++Project Status ++
		public IActionResult ProjectStatus(ProjectStatus status)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.ProjectStatusList = _db.ProjectStatus.OrderBy(s => s.Name).ToList();
			return View(status);
		}

		public IActionResult ExportProjectStatusList()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var pStatus_list = _db.ProjectStatus.OrderBy(s => s.Name).ToList();

			var columnHeader = new List<string>
			{
				"Name",
				"Description",
			};

			var fileContent = ExportExcelForStatusList(pStatus_list, columnHeader, "Project Status List");
			return File(fileContent, "application/ms-excel", "ProjectStatusList.xlsx");
		}


		public byte[] ExportExcelForStatusList(List<ProjectStatus> pStatus_list, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;
				using (var cells = worksheet.Cells[1, 1, 1, 8])
				{
					cells.Style.Font.Bold = true;
				}

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[1, i + 1].Value = columnHeader[i];
				}
				var j = 2;
				var count = 1;
				foreach (var item in pStatus_list)
				{
					worksheet.Cells["A" + j].Value = item.Name;
					worksheet.Cells["B" + j].Value = item.Description;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}


		public IActionResult SaveProjectStatus(ProjectStatus status)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (status.Id == Guid.Empty)
			{
				bool check = CheckForm(status.Name);
				if (!check)
				{
					ViewBag.ProjectStatusList = _db.ProjectStatus.OrderBy(s => s.Name).ToList();
					return View("ProjectStatus", status);
				}
				else
				{
					status.Id = Guid.NewGuid();
					status.CreatedDate = DateTime.Now;
					_db.Entry(status).State = EntityState.Added;
					_db.SaveChanges();
					TempData["Message"] = "Save Successful.";
					return RedirectToAction("ProjectStatus");
				}
			}
			else
			{
				bool check = CheckForm(status.Name);
				if (ModelState.IsValid && check == true)
				{
					_db.Update(status);
					_db.SaveChanges();
					TempData["Message"] = "Update Successful.";
					return RedirectToAction("ProjectStatus");
				}
			}
			ViewBag.ProjectStatusList = _db.ProjectStatus.OrderBy(s => s.Name).ToList();
			return View("ProjectStatus", status);
		}
		public IActionResult EditProjectStatus(Guid statusId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ProjectStatus status = _db.ProjectStatus.Where(r => r.Id == statusId).FirstOrDefault();
			ViewBag.ProjectStatusList = _db.ProjectStatus.OrderBy(s => s.Name).ToList();
			return View("ProjectStatus", status);
		}

		public IActionResult DeleteProjectStatus(Guid statusId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var status = _db.ProjectStatus.AsNoTracking().SingleOrDefault(m => m.Id == statusId);
			if (status == null)
			{
				ViewBag.ProjectStatusList = _db.ProjectStatus.OrderBy(s => s.Name).ToList();
				return View("ProjectStatus");
			}
			try
			{
				var projectList = _db.ProjectData.Where(u => u.ProjectStatusId == statusId).ToList();
				if (projectList.Count == 0)
				{
					_db.ProjectStatus.Remove(status);
					_db.SaveChanges();
					TempData["Message"] = "Delete Successful.";
					return RedirectToAction("ProjectStatus");
				}
				else
				{
					TempData["Message"] = "This project status has already used in process.";
				}
				ViewBag.ProjectStatusList = _db.ProjectStatus.OrderBy(s => s.Name).ToList();
				return View("ProjectStatus", status);
			}
			catch (DbUpdateException)
			{
				return View("ProjectStatus", new { id = statusId, saveChangesError = true });
			}
		}
		#endregion ++ Project Status ++

		public bool CheckLoginUser()
		{
			var userId = HttpContext.Session.GetString("userId");
			if (userId == null)
			{
				return false;
			}
			return true;

		}
		bool CheckForm(String name)
		{
			if (name == null)
			{
				TempData["Message"] = "*Please fill all the required fields";
				return false;
			}
			return true;
		}

	}
}