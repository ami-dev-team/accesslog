﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AMI.AccessLog.Project.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using AMI.AccessLog.Project.Data_Access;
using AMI.AccessLog.Project.Services;
using System.Data;
using System.IO;

namespace AMI.AccessLog.Project.Controllers
{
    public class HomeController : Controller
    {
        private IHostingEnvironment _env;
        public ISession session;
        AMI_ACCESSLOG_DBContext _db = new AMI_ACCESSLOG_DBContext();
        DataService dataService = new DataService();

        public HomeController(IHostingEnvironment env)
        {
            _env = env;
        }

        #region For Dashboard
        public IActionResult Dashboard()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            Dashboard model = new Dashboard();

            model.AccessLog = dataService.AccessLogCase(new RequestByFilter());
            model.ActivityLog = dataService.ActivityLogCase(new RequestByFilter());
            return View(model);
        }

        public IActionResult AccessLogList(DataSet ds)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            return View(ds);
        }

        public IActionResult ActivityLogList(DataSet ds)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            
            return View(ds);
        }

        public IActionResult AccessListByDate(DataSet ds)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            RequestByFilter request = HttpContext.Session.GetObjectFromJson<RequestByFilter>("ACfilter");
            
            if (request == null)
            {
                request = new RequestByFilter();
            }

            ds = dataService.AccessLogList(request);
            
            return View("AccessLogList", ds);
        }

        public IActionResult AccessLogListExcelExportForReport(Report model)
        {
            RequestByFilter request = HttpContext.Session.GetObjectFromJson<RequestByFilter>("ACfilter");
            RegisterViewModel VM = new RegisterViewModel();

            if (request == null)
            {
                request = new RequestByFilter();
                request.fromdate = DateTime.Now;
                request.todate = DateTime.Now;

                VM.AccessLogList = (from t1 in _db.Register
                                    join t3 in _db.Device on t1.DeviceId equals t3.Id
                                    join t4 in _db.SysUser on t1.SysUserId equals t4.Id
                                    orderby t1.Date
                                    select new RegisterViewModel
                                    {
                                        Id = t1.Id,
                                        Date = t1.Date,
                                        Device = t3.Name,
                                        Ip = t3.Ip,
                                        Reason = t1.Reason,
                                        TechnicianName = t4.Name,
                                        TechnicianReport = t1.TechnicianReport,
                                        Remark = t1.Remark
                                    }
             ).ToList();
            }
            else
            {
                VM.AccessLogList = (from t1 in _db.Register
                                    join t3 in _db.Device on t1.DeviceId equals t3.Id
                                    join t4 in _db.SysUser on t1.SysUserId equals t4.Id
                                    where t1.Date >= request.fromdate
                                    where t1.Date <= request.todate
                                    orderby t1.Date
                                    select new RegisterViewModel
                                    {
                                        Id = t1.Id,
                                        Date = t1.Date,
                                        Device = t3.Name,
                                        Ip = t3.Ip,
                                        Reason = t1.Reason,
                                        TechnicianName = t4.Name,
                                        TechnicianReport = t1.TechnicianReport,
                                        Remark = t1.Remark
                                    }
                  ).ToList();
            }

            VM.fromDate = model.fromDate;
            VM.toDate = model.toDate;
            VM.StaffName = _db.SysUser.Where(u => u.Id == model.userId).Select(u => u.Name).FirstOrDefault();
            var columnHeader = new List<string>
            {
                "Date/Time",
                "Equipment Device",
                "IP Address",
                "Reason of Configuration",
                "Technician Name",
                "Technician's Summary Report",
                "Remark",
            };

            var fileContent = ExportExcelByUserId(VM, columnHeader, "Report");
            return File(fileContent, "application/ms-excel", "Report of IT Equipment Configuration.xlsx");
        }

        public byte[] ExportExcelByUserId(RegisterViewModel VM, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;

                //worksheet.Cells[1, 1].Style.Font.Bold = true;
                //worksheet.Cells[1, 1].Value = "Staff Name : ";
                //worksheet.Cells[1, 2].Value = VM.StaffName;

                //worksheet.Cells[2, 1].Style.Font.Bold = true;
                //worksheet.Cells[2, 1].Value = "From Date :";
                //worksheet.Cells[2, 2].Value = string.Format("{0:dd/MMM/yyyy}", VM.fromDate);

                worksheet.Cells[1, 1].Style.Font.Bold = true;
                worksheet.Cells[1, 1].Value = "Date :";
                worksheet.Cells[1, 2].Value = string.Format("{0:dd/MMM/yyyy}", DateTime.Now);

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[3, i + 1].Style.Font.Bold = true;
                    worksheet.Cells[3, i + 1].Value = columnHeader[i];
                }
                var j = 4;
                var count = 1;
                foreach (var item in VM.AccessLogList)
                {
                    //DateTime? date = item.Date;
                    string asString = string.Format("{0:dd/MMM/yyyy}", item.Date);
                    worksheet.Cells["A" + j].Value = asString;
                    worksheet.Cells["B" + j].Value = item.Device;
                    worksheet.Cells["C" + j].Value = item.Ip;
                    worksheet.Cells["D" + j].Value = item.Reason;
                    worksheet.Cells["E" + j].Value = item.TechnicianName;
                    worksheet.Cells["F" + j].Value = item.TechnicianReport;
                    worksheet.Cells["G" + j].Value = item.Remark;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ExcelExportForActivity(Report model)
        {
            RequestByFilter request = HttpContext.Session.GetObjectFromJson<RequestByFilter>("ACfilter");
            DailyActivityVM VM = new DailyActivityVM();

            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            if (request == null)
            {
                request = new RequestByFilter();
                request.fromdate = DateTime.Now;
                request.todate = DateTime.Now;

                VM.DailyActivitySummaryList = (from t1 in _db.DailyActivity
                                        join t4 in _db.SysUser on t1.UserId equals t4.Id
                                        orderby t1.CreatedDate ascending
                                        select new DailyActivitySummaryViewModel
                                        {
                                            Id = t1.Id,
                                            Name = t4.Name,
                                            FinishedTask = t1.FinishedTask,
                                            ContinueTask = t1.ContinueTask,
                                            CreatedDate = t1.CreatedDate,
                                            //UserId = model.userId
                                        }).ToList();
            }
            else
            {
                VM.DailyActivitySummaryList = (from t1 in _db.DailyActivity
                                        join t4 in _db.SysUser on t1.UserId equals t4.Id
                                        where t1.CreatedDate >= request.fromdate
                                        where t1.CreatedDate <= request.todate
                                        orderby t1.CreatedDate ascending
                                        select new DailyActivitySummaryViewModel
                                        {
                                            Id = t1.Id,
                                            Name = t4.Name,
                                            FinishedTask = t1.FinishedTask,
                                            ContinueTask = t1.ContinueTask,
                                            CreatedDate = t1.CreatedDate,
                                            //UserId = model.userId
                                        }).ToList();
            }

            VM.fromDate = model.fromDate;
            VM.toDate = model.toDate;
            VM.UserName = _db.SysUser.Where(u => u.Id == model.userId).Select(u => u.Name).FirstOrDefault();
            var columnHeader = new List<string>
            {
                "Date/Time",
                "Name",
                "Finished Task",
                "Continue Task",
            };

            var fileContent = ActivityExportExcelByUserId(VM, columnHeader, "Activity Report");
            return File(fileContent, "application/ms-excel", "Daily Activity.xlsx");
        }

        public byte[] ActivityExportExcelByUserId(DailyActivityVM VM, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;

                //worksheet.Cells[1, 1].Style.Font.Bold = true;
                //worksheet.Cells[1, 1].Value = "Staff Name : ";
                //worksheet.Cells[1, 2].Value = VM.UserName;

                //worksheet.Cells[2, 1].Style.Font.Bold = true;
                //worksheet.Cells[2, 1].Value = "From Date :";
                //worksheet.Cells[2, 2].Value = string.Format("{0:dd/MMM/yyyy}", VM.fromDate);

                worksheet.Cells[1, 1].Style.Font.Bold = true;
                worksheet.Cells[1, 1].Value = "Date :";
                worksheet.Cells[1, 2].Value = string.Format("{0:dd/MMM/yyyy}", DateTime.Now);

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[3, i + 1].Style.Font.Bold = true;
                    worksheet.Cells[3, i + 1].Value = columnHeader[i];
                }
                var j = 4;
                var count = 1;
                foreach (var item in VM.DailyActivitySummaryList)
                {
                    string asString = string.Format("{0:dd/MMM/yyyy}", item.CreatedDate);
                    worksheet.Cells["A" + j].Value = asString;
                    worksheet.Cells["B" + j].Value = item.Name;
                    worksheet.Cells["C" + j].Value = item.FinishedTask;
                    worksheet.Cells["D" + j].Value = item.ContinueTask;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ActivityLogListByDate(DataSet ds)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            RequestByFilter request = HttpContext.Session.GetObjectFromJson<RequestByFilter>("ACfilter");
            if (request == null)
            {
                request = new RequestByFilter();
            }
            ViewBag.Request = request;
            
            ds = dataService.ActivityLogList(request);
            
            return View("ActivityLogList", ds);
        }

        [HttpPost]
        public JsonResult AccessLogByDate([FromBody] RequestByFilter request)
        {
            HttpContext.Session.SetObjectAsJson("ACfilter", null);
            if (request != null)
            {
                HttpContext.Session.SetObjectAsJson("ACfilter", request);

                Dashboard model = new Dashboard();
             
                model.AccessLog = dataService.AccessLogCase(request);
                model.ActivityLog = dataService.ActivityLogCase(request);

                return Json(model);
            }
            return Json(new Dashboard());
        }
        
        public JsonResult GetDDLData(Guid Id)
        {
            List<Device> DeviceList = new List<Device>();
            if (Id != Guid.Empty)
            {
                DeviceList = (from t in _db.Device
                                where t.Id == Id
                                select new Device
                                {
                                    Id = t.Id,
                                    Name = t.Name
                                }).OrderBy(t => t.Name).ToList();
            }
            else
            {
                DeviceList = (from t in _db.Device
                              select new Device
                              {
                                  Id = t.Id,
                                  Name = t.Name
                              }).OrderBy(t => t.Name).ToList();
            }
            return Json(DeviceList);
        }

        #region ++ Total Project ++
        public JsonResult GetTotalProjectCount()
        {
            var result = Json((from h in _db.ProjectData
                               select h).Count());
            return result;
        }

        public JsonResult GetCompletedProjectCount()
        {
            var result = Json((from t1 in _db.ProjectData
                               join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id
                               where (t2.Name == "100%")
                               select t1).Count());
            return result;
        }

        public JsonResult GetOngoingProjectCount()
        {
            var result = Json((from t1 in _db.ProjectData
                               join t2 in _db.ProjectStatus on t1.ProjectStatusId equals t2.Id
                               where (t2.Name != "100%")
                               select t1).Count());
            return result;
        }

        public JsonResult GetPjStatusNullProjectCount()
        {
            var result = Json((from h in _db.ProjectData
                               where (h.ProjectStatusId == null)
                               select h).Count());
            return result;
        }
        
        #endregion

        #endregion

        void GetViewBagDataForAccesslog(Guid UserId)
		{
			ViewBag.RegisterList = (from t1 in _db.Register
									join t3 in _db.Device on t1.DeviceId equals t3.Id
									join t4 in _db.SysUser on t1.SysUserId equals t4.Id
									where t4.Id == UserId
									//orderby t1.Date descending
									orderby t1.CreatedDate descending
									select new RegisterViewModel
									{
										Id = t1.Id,
										UserId = UserId,
										Date = t1.Date,
										Device = t3.Name,
										Reason = t1.Reason,
										TechnicianName = t4.Name,
										TechnicianReport = t1.TechnicianReport,
										Remark = t1.Remark
									}
			   ).ToList();
			ViewBag.DeviceList = new SelectList(_db.Device.OrderBy(d => d.Name).ToList(), "Id", "Name");
            ViewBag.DeviceIPList = new SelectList((_db.Device.Where(p=> p.Ip !=null).OrderBy(p => p.Ip)).ToList(), "Id", "Ip");
        }

		public IActionResult Register(Register register)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			GetViewBagDataForAccesslog(Guid.Parse(HttpContext.Session.GetString("userId")));
            return View(register);
        }

        public IActionResult Delete(Guid RegisterId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var register = _db.Register.AsNoTracking().SingleOrDefault(m => m.Id == RegisterId);
            if (register == null)
            {
				ViewBag.DeviceList = new SelectList(_db.Device.OrderBy(d => d.Name).ToList(), "Id", "Name");
				return View("Register");
            }

            try
            {
                _db.Register.Remove(register);
                _db.SaveChanges();
				TempData["Message"] = "Delete Successful.";
				return RedirectToAction("Register");
			}
			catch (DbUpdateException)
            {
                return View("Register", new { id = RegisterId, saveChangesError = true });
            }
        }

        bool CheckRegisterUserForm(Register model)
        {
            if (model.Date == null || model.DeviceId == Guid.Empty || model.Reason == null)
            {
                TempData["Message"] = "*Please fill all the required fields";
                return false;
            }
            //if (model.Date == null || model.Reason == null)
            //{
            //    TempData["Message"] = "*Please fill all the required fields";
            //    return false;
            //}
            return true;
        }
        
        public IActionResult Save(Register model)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			try
			{
                if (model.Id == Guid.Empty)
                {
                    bool check = CheckRegisterUserForm(model);
                    if (!check)
                    {
                        //ViewBag.DeviceList = new SelectList(_db.Device.OrderBy(d => d.Name).ToList(), "Id", "Name");
                        //ViewBag.DeviceIPList = new SelectList((_db.Device.Where(p => p.Ip != null).OrderBy(p => p.Ip)).ToList(), "Id", "IP");
                        //return View("Register", model);
                        GetViewBagDataForAccesslog(Guid.Parse(HttpContext.Session.GetString("userId")));
                        return View("Register", model);
                    }

					else
                    {
                        model.Id = Guid.NewGuid();
                        model.Date = model.Date;
						model.CreatedDate = DateTime.Now;
						model.SysUserId = Guid.Parse(HttpContext.Session.GetString("userId"));

                        _db.Entry(model).State = EntityState.Added;
                        _db.SaveChanges();
                        TempData["Message"] = "Save Successful.";
						return RedirectToAction("Register");
					}
				}
                else
                {
                    bool check = CheckRegisterUserForm(model);
                    if (ModelState.IsValid && check == true)
                    {
                        model.SysUserId = Guid.Parse(HttpContext.Session.GetString("userId"));
						model.Date = model.Date;
						_db.Register.Update(model);
                        _db.SaveChanges();
                        TempData["Message"] = "Update Successful.";
						return RedirectToAction("Register");
					}
				}
            }
            catch (DbUpdateException ex)
            {
                TempData["Message"] = "Error in Saving Process.";
                return RedirectToAction("Register", model);
            }
			ViewBag.DeviceList = new SelectList(_db.Device.OrderBy(d => d.Name).ToList(), "Id", "Name");
			return View("Register", model);
		}

		public bool CheckLoginUser()
        {
            var userId = HttpContext.Session.GetString("userId");
            if (userId == null)
            {
                return false;
            }
            return true;

        }

        public IActionResult Edit(Guid Id)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			Register register = _db.Register.Where(e => e.Id == Id).FirstOrDefault();
			GetViewBagDataForAccesslog(Guid.Parse(HttpContext.Session.GetString("userId")));
			return View("Register", register);
        }

        public IActionResult ExcelExport()
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			session = HttpContext.Session;
			var UserId = Guid.Parse(session.GetString("userId"));
			var userInfos = (from t1 in _db.Register
									join t3 in _db.Device on t1.DeviceId equals t3.Id
									join t4 in _db.SysUser on t1.SysUserId equals t4.Id
									where t4.Id == UserId
									orderby t1.Date descending
									select new RegisterViewModel
									{
										Id = t1.Id,
										Date = t1.Date,
										Device = t3.Name,
										Reason = t1.Reason,
										TechnicianName = t4.Name,
										TechnicianReport = t1.TechnicianReport,
										Remark = t1.Remark
									}
						   ).ToList();
            var columnHeader = new List<string>
            {
                "Date/Time",
                "Equipment Device",
                "Reason of Configuration",
                "Technician Name",
                "Technician's Summary Report",
                "Remark"
            };

            var fileContent = ExportExcelForRegisterList(userInfos, columnHeader, "IT Equipment Configuration List");
            return File(fileContent, "application/ms-excel", "RegisterList.xlsx");
        }

        public byte[] ExportExcelForRegisterList(List<RegisterViewModel> registerList, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 9])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in registerList)
                {
					worksheet.Cells["A" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.Date);
                    worksheet.Cells["B" + j].Value = item.Device;
                    worksheet.Cells["C" + j].Value = item.Reason;
                    worksheet.Cells["D" + j].Value = item.TechnicianName;
                    worksheet.Cells["E" + j].Value = item.TechnicianReport;
                    worksheet.Cells["F" + j].Value = item.Remark;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        #region CoreTeam Excel
        public IActionResult ServiceData(ServiceData model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            
            ViewBag.BranchList = new SelectList(_db.Branch.OrderBy(r => r.BranchName).ToList(), "Id", "BranchName");
            ViewBag.DepartmentList = new SelectList(_db.Department.OrderBy(r => r.DepartmentName).ToList(), "Id", "DepartmentName");
            ViewBag.ChannelList = new SelectList(_db.Channel.OrderBy(r => r.ChannelName).ToList(), "Id", "ChannelName");
            ViewBag.ServicePersonList = new SelectList(_db.ServicePerson.OrderBy(r => r.PersonName).ToList(), "Id", "PersonName");
            ViewBag.IssueTypeList = new SelectList(_db.IssueType.OrderBy(r => r.IssueTypeCode).ToList(), "Id", "IssueTypeName");
            ViewBag.ServiceStatusList = new SelectList(_db.ServiceStatus.OrderBy(r => r.StatusCode).ToList(), "Id", "StatusName");
            ViewBag.ServiceDataList = (from sd in _db.ServiceData
                                         join b in _db.Branch on sd.BranchId equals b.Id
                                         join d in _db.Department on sd.DepartmentId equals d.Id
                                         join c in _db.Channel on sd.ChannelId equals c.Id
                                         join sp in _db.ServicePerson on sd.ServicePersonId equals sp.Id
                                         join iss in _db.IssueType on sd.IssueType equals iss.Id
                                         join st in _db.ServiceStatus on sd.Status equals st.Id
                                       orderby sd.ServiceDate descending
                                         select new ServiceDataViewModel
                                         {
                                             Id = sd.Id,
                                             ServiceDate = sd.ServiceDate,
                                             UserName = sd.UserName,
                                             BranchName = b.BranchName,
                                             DepartmentName = d.DepartmentName,
                                             ReasonofRequest = sd.ReasonofRequest,
                                             ChannelName = c.ChannelName,
                                             Solution = sd.Solution,
                                             ServicePersonName = sp.PersonName,
                                             IssueTypeName = iss.IssueTypeName,
                                             Ticket = sd.Ticket,
                                             StatusName = st.StatusName,
                                             Remark = sd.Remark
                                         }).ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult SaveServiceData(ServiceData model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            try
            {
                if (model.Id == Guid.Empty)
                {
                    model.Id = Guid.NewGuid();
                    _db.Entry(model).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successfully!";
                    return RedirectToAction("ServiceData");
                }
                else
                {
                    _db.Update(model);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successfully!";
                    return RedirectToAction("ServiceData");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("ServiceData");
            }
        }

        public IActionResult EditServiceData(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ServiceData sdata = new ServiceData();
            sdata = _db.ServiceData.Where(x => x.Id == Id).FirstOrDefault();

            ViewBag.BranchList = new SelectList(_db.Branch.OrderBy(r => r.BranchName).ToList(), "Id", "BranchName");
            ViewBag.DepartmentList = new SelectList(_db.Department.OrderBy(r => r.DepartmentName).ToList(), "Id", "DepartmentName");
            ViewBag.ChannelList = new SelectList(_db.Channel.OrderBy(r => r.ChannelName).ToList(), "Id", "ChannelName");
            ViewBag.ServicePersonList = new SelectList(_db.ServicePerson.OrderBy(r => r.PersonName).ToList(), "Id", "PersonName");
            ViewBag.IssueTypeList = new SelectList(_db.IssueType.OrderBy(r => r.IssueTypeCode).ToList(), "Id", "IssueTypeName");
            ViewBag.ServiceStatusList = new SelectList(_db.ServiceStatus.OrderBy(r => r.StatusCode).ToList(), "Id", "StatusName");

            ViewBag.ServiceDataList = (from sd in _db.ServiceData
                                       join b in _db.Branch on sd.BranchId equals b.Id
                                       join d in _db.Department on sd.DepartmentId equals d.Id
                                       join c in _db.Channel on sd.ChannelId equals c.Id
                                       join sp in _db.ServicePerson on sd.ServicePersonId equals sp.Id
                                       join iss in _db.IssueType on sd.IssueType equals iss.Id
                                       join st in _db.ServiceStatus on sd.Status equals st.Id
                                       orderby sd.ServiceDate descending
                                       select new ServiceDataViewModel
                                       {
                                           Id = sd.Id,
                                           ServiceDate = sd.ServiceDate,
                                           UserName = sd.UserName,
                                           BranchName = b.BranchName,
                                           DepartmentName = d.DepartmentName,
                                           ReasonofRequest = sd.ReasonofRequest,
                                           ChannelName = c.ChannelName,
                                           Solution = sd.Solution,
                                           ServicePersonName = sp.PersonName,
                                           IssueTypeName = iss.IssueTypeName,
                                           Ticket = sd.Ticket,
                                           StatusName = st.StatusName,
                                           Remark = sd.Remark
                                       }).ToList();

            return View("ServiceData", sdata);
        }

        public IActionResult DeleteServiceData(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var sd = _db.ServiceData.AsNoTracking().SingleOrDefault(m => m.Id == Id);
            if (sd == null)
            {
                return View("ServiceData", sd);
            }

            try
            {
                ServiceData sdata = _db.ServiceData.Where(u => u.Id == Id).FirstOrDefault();
                if (sdata != null)
                {
                    _db.ServiceData.Remove(sdata);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successful.";
                    return RedirectToAction("ServiceData", new ServiceData());
                }
                //else
                //{
                //    TempData["Message"] = "This location has already used in process.";
                //}
                return RedirectToAction("ServiceData", new ServiceData());
            }
            catch (DbUpdateException)
            {
                return View("ServiceData", new { id = Id, saveChangesError = true });
            }
        }

        public IActionResult ExportServiceDataList()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var ServiceDataList = (from sd in _db.ServiceData
                                       join b in _db.Branch on sd.BranchId equals b.Id
                                       join d in _db.Department on sd.DepartmentId equals d.Id
                                       join c in _db.Channel on sd.ChannelId equals c.Id
                                       join sp in _db.ServicePerson on sd.ServicePersonId equals sp.Id
                                       join iss in _db.IssueType on sd.IssueType equals iss.Id
                                       join st in _db.ServiceStatus on sd.Status equals st.Id
                                       orderby sd.ServiceDate descending
                                       select new ServiceDataViewModel
                                       {
                                           Id = sd.Id,
                                           ServiceDate = sd.ServiceDate,
                                           UserName = sd.UserName,
                                           BranchName = b.BranchName,
                                           DepartmentName = d.DepartmentName,
                                           ReasonofRequest = sd.ReasonofRequest,
                                           ChannelName = c.ChannelName,
                                           Solution = sd.Solution,
                                           ServicePersonName = sp.PersonName,
                                           IssueTypeName = iss.IssueTypeName,
                                           Ticket = sd.Ticket,
                                           StatusName = st.StatusName,
                                           Remark = sd.Remark
                                       }).ToList();

            var columnHeader = new List<string>
            {
                "ServiceDate",
                "UserName",
                "Branch",
                "Department",
                "Reason of Report",
                "Channel",
                "Solution",
                "Service Person Name",
                "Issue Type",
                "RT No",
                "Status",
                "Remark"
            };

            var fileContent = ExportExcelForServicedataList(ServiceDataList, columnHeader, "Service Data List");
            return File(fileContent, "application/ms-excel", "ServiceDataList.xlsx");
        }

        public byte[] ExportExcelForServicedataList(List<ServiceDataViewModel> ServiceDataList, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 12])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in ServiceDataList)
                {
                    worksheet.Cells["A" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ServiceDate);
                    worksheet.Cells["B" + j].Value = item.UserName;
                    worksheet.Cells["C" + j].Value = item.BranchName;
                    worksheet.Cells["D" + j].Value = item.DepartmentName;
                    worksheet.Cells["E" + j].Value = item.ReasonofRequest;
                    worksheet.Cells["F" + j].Value = item.ChannelName;
                    worksheet.Cells["G" + j].Value = item.Solution;
                    worksheet.Cells["H" + j].Value = item.ServicePersonName;
                    worksheet.Cells["I" + j].Value = item.IssueTypeName;
                    worksheet.Cells["J" + j].Value = item.Ticket;
                    worksheet.Cells["K" + j].Value = item.StatusName;
                    worksheet.Cells["L" + j].Value = item.Remark;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ImportCoreDataList()
        {
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public IActionResult ImportCoreDataList(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                try
                {
                    string folderName = "ImportedFiles";
                    string webRootPath = _env.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories

                    if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                    using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                    {
                        reportfile.CopyTo(fileStream);
                    }
                    // Get uploaded file path with root
                    string rootFolder = _env.WebRootPath;
                    string fileName = @"ImportedFiles/" + fiName;
                    FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                        int totalRows = workSheet.Dimension.Rows;
                        List<ServiceData> SDList = new List<ServiceData>();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            ServiceData sp = new ServiceData();
                            sp.Id = Guid.NewGuid();
                            sp.ServiceDate = Convert.ToDateTime(workSheet.Cells[i, 1].Value).Date;
                            sp.UserName = workSheet.Cells[i, 2].Value.ToString();
                            
                            string branchCode = workSheet.Cells[i, 3].Value.ToString();

                            Guid branchId = (from x in _db.Branch
                                               where x.BranchCode == branchCode
                                           select x.Id).FirstOrDefault();

                            sp.BranchId = branchId;

                            string depaertmentCode = workSheet.Cells[i, 4].Value.ToString();

                            Guid deptId = (from x in _db.Department
                                           where x.DepartmentCode == depaertmentCode
                                           select x.Id).FirstOrDefault();
                            sp.DepartmentId = deptId;

                            sp.ReasonofRequest = workSheet.Cells[i, 5].Value.ToString();

                            string channelCode = workSheet.Cells[i, 6].Value.ToString();

                            Guid channelId = (from x in _db.Channel
                                             where x.ChannelCode == channelCode
                                             select x.Id).FirstOrDefault();

                            sp.ChannelId = channelId;
                            sp.Solution = workSheet.Cells[i, 7].Value.ToString();

                            string servicepersonCode = workSheet.Cells[i, 8].Value.ToString();

                            Guid splId = (from x in _db.ServicePerson
                                              where x.PersonId == servicepersonCode
                                              select x.Id).FirstOrDefault();

                            sp.ServicePersonId = splId;

                            string issCode = workSheet.Cells[i, 9].Value.ToString();

                            Guid issId = (from x in _db.IssueType
                                          where x.IssueTypeCode == issCode
                                          select x.Id).FirstOrDefault();

                            sp.IssueType = issId;
                            
                            if (workSheet.Cells[i, 10].Value != null)
                            {
                                sp.Ticket = workSheet.Cells[i, 10].Value.ToString();
                            }
                            else
                            {
                                sp.Ticket = "";
                            }

                            string statusCode = workSheet.Cells[i, 11].Value.ToString();

                            Guid statusId = (from x in _db.ServiceStatus
                                          where x.StatusCode == statusCode
                                          select x.Id).FirstOrDefault();

                            sp.Status = statusId;
                            
                            if (workSheet.Cells[i, 12].Value != null)
                            {
                                sp.Remark = workSheet.Cells[i, 12].Value.ToString();
                            }
                            else
                            {
                                sp.Remark = "";
                            }
                            SDList.Add(sp);
                        }
                        _db.ServiceData.AddRange(SDList);
                        _db.SaveChanges();
                        TempData["Message"] = "Import Data Successfully.";
                        return RedirectToAction("ServiceData");
                    }
                }
                catch (Exception ex)
                {
                    TempData["Message"] = ex.Message.ToString();
                    return RedirectToAction("ServiceData");
                }
            }

        }

        #endregion
    }
}


