﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AMI.AccessLog.Project.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;

namespace AMI.AccessLog.Project.Controllers
{
    public class AccountController : Controller
    {
        AMI_ACCESSLOG_DBContext _db = new AMI_ACCESSLOG_DBContext();

        private IHostingEnvironment _env;

        Common common = new Common();

        public AccountController(IHostingEnvironment env)
        {
            _env = env;

            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }

        public ISession session;

        public IConfigurationRoot Configuration { get; }

		public IActionResult QRCode()
		{
			QRCode model = new QRCode();
			//model.AuthenticatorUri = "https://apps.apple.com/us/app/aya-myanmar-insurance/id1404215060";
			model.AuthenticatorUri = "https://play.google.com/store/apps/details?id=com.ami.amiinsurance.live";
			return View(model);
		}

		public IActionResult Login(SysUser model)
        {
            return View(model);
        }

		public IActionResult Profile()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			Guid userId = Guid.Parse(HttpContext.Session.GetString("userId"));
			UserInfo data = (from u in _db.SysUser
								join r in _db.Role on u.Role equals r.RoleId
								join p in _db.Position on u.Position equals p.Id
								where u.Id == userId
								orderby u.Name
								select new UserInfo
								{
									Id = u.Id,
									Name = u.Name,
									Email = u.Email,
									Position = p.Name,
								}).FirstOrDefault();
			return View(data);
		}

		public IActionResult ChangeProfile()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			session = HttpContext.Session;
			Guid userId = Guid.Parse(session.GetString("userId"));
			UserInfo data = (from u in _db.SysUser
							 join r in _db.Role on u.Role equals r.RoleId
							 join p in _db.Position on u.Position equals p.Id
							 where u.Id == userId
							 orderby u.Name
							 select new UserInfo
							 {
								 Id = u.Id,
								 Name = u.Name,
								 Email = u.Email,
								 Position = p.Name,
							 }).FirstOrDefault();
			return View(data);
		}


		[HttpPost]
        //public IActionResult SignIn(string email, string password)
        public IActionResult SignIn(SysUser model)
        {
            SysUser user = _db.SysUser.Where(u => u.Email == model.Email).FirstOrDefault();
            if (user != null && user.Id != null && user.IsActive == true)
            {
                string Encodepassword = common.DecryptPassword(user.Password, user.PasswordSalt);

				HttpContext.Session.SetString("email", user.Email);
				HttpContext.Session.SetString("name", user.Name);
				HttpContext.Session.SetString("userId", Convert.ToString(user.Id));
				HttpContext.Session.SetString("roleId", Convert.ToString(user.Role));
				if (model.Password == "12345678" && user.FirstTimeLogin == true)
                {
                    return RedirectToAction("ChangePasswordForFirstTimeLogin");
                }
                else if (Encodepassword == model.Password)
                {
                    // return RedirectToAction("Register","Home");
                    return RedirectToAction("Dashboard", "Home");
                }
                else
                {
                    TempData["LoginMessage"] = "Wrong password.Try again or click Forgot password to reset it.";
                    return View("Login",model);
                }
            }
			//User is not active
			else if (user != null && user.Id != null && user.IsActive == false)
			{
				TempData["LoginMessage"] = "You are not active. Please contact to administrator.";
				return View("Login", model);
			}
			//User is not register
			else
			{
				TempData["LoginMessage"] = "You are not registerd. Please contact to administrator.";
				return View("Login", model);
			}
        }

        public IActionResult SignOut()
        {
            ViewBag.login = null;
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }

        public IActionResult Register()
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.RoleList = new SelectList(_db.Role.ToList(), "RoleId", "Description");
            return View();
        }

        [HttpPost]
        public IActionResult Register(SysUser model)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			return View();
        }

        public bool CheckLoginUser()
        {
            var userId = HttpContext.Session.GetString("userId");
            if (userId == null)
            {
                return false;
            }
            return true;

        }
        public IActionResult ChangePassword() //Change Password(Show Change Password Form)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			return View();
        }

        public IActionResult ChangePasswordForFirstTimeLogin()
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			return View();
        }

        [HttpPost] //Change Password
        public IActionResult ChangePasswordForFirstTimeLogin(string npassword, string opassword, string ConfirmPassword)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login");

			string oldPassword = "";
            Guid userId = Guid.Parse(HttpContext.Session.GetString("userId"));

            if (npassword == ConfirmPassword)
            {
                var user = _db.SysUser.Where<SysUser>(u => u.Id == userId).FirstOrDefault();
                oldPassword = common.DecryptPassword(user.Password, user.PasswordSalt);
                if (opassword == npassword)
                {
					ViewBag.Message = "System doesn't allow your password because oldPwd and  newPwd are same.";
					return View();
                }
                else if (oldPassword == opassword)
                {
                    user.Password = common.EncryptPassword(npassword, user.PasswordSalt);
                    user.FirstTimeLogin = false;
                    _db.SaveChanges();
                    return RedirectToAction("Login");
                }
                else if (oldPassword != opassword)
                {
                    ViewBag.Message = "Your old password is wrong.";
                    return View();
                }
                else
                {
                    ViewBag.Message = "Password does not match.";
                    return View();
                }
            }
            else
            {
                ViewBag.Message = "New Password & Confirm Password does not match";
                return View();
            }
        }

        [HttpPost] //Change Password
        public IActionResult ChangePassword(string npassword, string opassword, string ConfirmPassword)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			session = HttpContext.Session;
            var id = session.GetString("userId");
            string DBoldPassword = "";
            Guid uid = new Guid();
            Guid.TryParse(id, out uid);

            string email = session.GetString("user");
            if (npassword == ConfirmPassword)
            {
                var user = _db.SysUser.Where<SysUser>(u => u.Id == uid).FirstOrDefault();
                DBoldPassword = common.DecryptPassword(user.Password, user.PasswordSalt);
                if (DBoldPassword == npassword)
				{
					TempData["Message"] = "*System doesn't allow your password because oldPwd and  newPwd are same.";
					return View();
				}
                else if (DBoldPassword != opassword)
				{
					TempData["Message"] = "*Old Password does not match.";
					return View();
				}
				//else if (DBoldPassword == opassword)
				//{
				//	user.Password = common.EncryptPassword(npassword, user.PasswordSalt);
				//	_db.SaveChanges();
				//	return RedirectToAction("Login");
				//}
				else 
				{
					user.Password = common.EncryptPassword(npassword, user.PasswordSalt);
					_db.SaveChanges();
					return RedirectToAction("Login");
				}
			}
            else
            {
				TempData["Message"] = "*New Password & Confirm Password does not match";
                return View();
            }
        }
        public IActionResult ForgotPassword() //Change Password(Show Change Password Form)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login");

			return View();
        }

        [HttpPost] //Reset Password
        public IActionResult SendEmail(string Email)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login");

			SysUser user = new SysUser();

            if (Email != string.Empty)
            {
                var myMessage = new MailMessage();

                var FromEmail = Configuration["PswResetCodeSender:Email"];

                MailAddress fromAddress = new MailAddress(FromEmail, "Sender");
                MailAddress toAddress = new MailAddress(Email, "Recipient");

                myMessage.From = fromAddress;
                myMessage.To.Add(toAddress);
                user = _db.SysUser.Where(u => u.Email == Email).FirstOrDefault();

                if (user == null)
                {
                    TempData["Incorrect Email"] = "Please enter a correct email";
                    return RedirectToAction("ForgotPassword");
                } ////send email(below getting data for user from database)

                string ConfirmationCode = GetRandomKey(6);
                if (user != null)
                {
                    string UserId = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Convert.ToString(user.Id)));
                    myMessage.Subject = "Reset Password Link";
                    string Body = "Hello " + user.Name + ",";
                    Body += "<br /><br />This code is to activate your account : " + " <strong>" + ConfirmationCode + "</strong>";
                    Body += "<br /><br /><br />";
                    myMessage.IsBodyHtml = true;
                    myMessage.Body = Body;
                }

                SmtpClient client = new SmtpClient("172.20.172.173", 25);
                client.UseDefaultCredentials = true;
                NetworkCredential networkCredential = CredentialCache.DefaultNetworkCredentials;
                client.Credentials = networkCredential.GetCredential("172.20.172.173", 25, "Basic");
                client.Send(myMessage);

                //SmtpClient smtp = new SmtpClient("172.20.172.173",25);
                //NetworkCredential NetworkCred = new NetworkCredential("theingiw@ami-insurance.com", "@dmin@123");
                //smtp.UseDefaultCredentials = false;
                //smtp.Credentials = NetworkCred;
                //smtp.Send(myMessage);

                TempData["ConfrimCodeMsg"] = "Confirmation code has already sent.";

                user.ConfirmCode = ConfirmationCode;
                _db.Update(user);
                _db.SaveChanges();
            }
            return RedirectToAction("SendCode", new { @userId = user.Id });
        }

        private string GetRandomKey(int len)
        {
            int maxSize = len;
            char[] chars = new char[30];
            string a = "1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data) { result.Append(chars[b % (chars.Length)]); }
            return result.ToString();
        }

        public IActionResult ResetPassword(Guid userId) //Change Password(Show Change Password Form)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login");

			ViewBag.userId = userId;
            return View();
        }

        [HttpPost]
        public IActionResult ResetPassword(string NewPassword, string ConfirmPassword, Guid UserId) //Change Password(Show Change Password Form)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login");

			if (NewPassword != ConfirmPassword)
            {
                ViewBag.Message = "Password and confirm password do not match.";
                ViewBag.userId = UserId;
            }
            else
            {
                var user = _db.SysUser.Where<SysUser>(u => u.Id == UserId).FirstOrDefault();
                if (user != null)
                {
                    user.Password = common.EncryptPassword(NewPassword, user.PasswordSalt);
                    _db.SaveChanges();
                    return RedirectToAction("Login");
                }
            }
            return View();
        }

        public IActionResult SendCode(Guid userId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login");

			ViewBag.userId = userId;
            return View();
        }

        [HttpPost]
        public IActionResult SendCode(string ConfirmCode, Guid userId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login");

			var user = from u in _db.SysUser
                       where u.Id == userId &&
                        u.ConfirmCode == ConfirmCode
                       select u;
            if (user == null || user.Count() == 0)
            {
                ViewBag.Message = "Your confirmation code is wrong. Try again.";
                ViewBag.userId = userId;
                return View();
            }
            return RedirectToAction("ResetPassword", new { userId });
        }

		public IActionResult CheckUserInfo(Guid userId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login");

			UserInfo data = (from u in _db.SysUser
							 join r in _db.Role on u.Role equals r.RoleId
							 join p in _db.Position on u.Position equals p.Id
							 where u.Id == userId
							 orderby u.Name
							 select new UserInfo
							 {
								 Id = u.Id,
								 Name = u.Name,
								 Email = u.Email,
								 Position = p.Name,
								 Role = r.RoleName,
								 IsActive = u.IsActive
							 }).FirstOrDefault();
			return View(data);
		}

		public IActionResult ResetUser(Guid userId)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login");

			SysUser model = _db.SysUser.Where(u => u.Id == userId).FirstOrDefault();
			model.FirstTimeLogin = true;
			model.PasswordSalt = common.GenerateSalt(8);
			model.Password = common.EncryptPassword("12345678", model.PasswordSalt);
			model.IsActive = true;
			_db.Update(model);
			_db.SaveChanges();
			TempData["Message"] = "Reset Password for ' " + model.Name + " ' is successful.";
			return RedirectToAction("UserList", "Admin");
		}

	}
}