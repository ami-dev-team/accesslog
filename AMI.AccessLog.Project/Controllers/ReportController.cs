﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AMI.AccessLog.Project.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using OfficeOpenXml;

namespace AMI.AccessLog.Project.Controllers
{
    public class ReportController : Controller
    {
		AMI_ACCESSLOG_DBContext _db = new AMI_ACCESSLOG_DBContext();
		Common common = new Common();

		private IHostingEnvironment _env;
		public ISession session;

		public ReportController(IHostingEnvironment env)
		{
			_env = env;
		}

		public bool CheckLoginUser()
		{
			var userId = HttpContext.Session.GetString("userId");
			if (userId == null)
			{
				return false;
			}
			return true;

		}

		#region ++ Accesslog Report ++
		public IActionResult AccessReportsByTeam(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.TeamList = new SelectList(_db.Team.OrderBy(u => u.Name).Distinct().ToList(), "Id", "Name");
			return View(model);
		}

		[HttpPost]
		public IActionResult GetAccessReportsByTeam(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.RegisterList = (from t1 in _db.Register
									join t3 in _db.Device on t1.DeviceId equals t3.Id
									join t4 in _db.SysUser on t1.SysUserId equals t4.Id
									join t2 in _db.Team on t4.TeamId equals t2.Id
									where t2.Id == model.TeamId
									where t1.Date >= model.fromDate
									where t1.Date <= model.toDate.AddHours(23.50)
									orderby t1.Date ascending
									select new RegisterViewModel
									{
										Id = t1.Id,
										Date = t1.Date,
										Device = t3.Name,
										Reason = t1.Reason,
										TechnicianName = t4.Name,
										TechnicianReport = t1.TechnicianReport,
										Remark = t1.Remark
									}).ToList();
			ViewBag.TeamName = _db.Team.Where(u => u.Id == model.TeamId).Select(s => s.Name).FirstOrDefault();
			ViewBag.fromDate = model.fromDate;
			ViewBag.toDate = model.toDate;
			return View(model);
		}

		[HttpPost]
		public IActionResult ExportAccessLogByTeam(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			RegisterViewModel VM = new RegisterViewModel();
			VM.AccessLogList = (from t1 in _db.Register
									join t3 in _db.Device on t1.DeviceId equals t3.Id
									join t4 in _db.SysUser on t1.SysUserId equals t4.Id
									join t2 in _db.Team on t4.TeamId equals t2.Id
									where t2.Id == model.TeamId
									where t1.Date >= model.fromDate
									where t1.Date <= model.toDate.AddHours(23.50)
									orderby t1.Date ascending
									select new RegisterViewModel
									{
										Id = t1.Id,
										Date = t1.Date,
										Device = t3.Name,
										Reason = t1.Reason,
										TechnicianName = t4.Name,
										TechnicianReport = t1.TechnicianReport,
										Remark = t1.Remark
									}).ToList();
			VM.TeamName = _db.Team.Where(u => u.Id == model.TeamId).Select(s => s.Name).FirstOrDefault();
			VM.fromDate =  model.fromDate;
			VM.toDate = model.toDate;
			var columnHeader = new List<string>
			{
				"Date/Time",
				"Equipment Device",
				"Reason of Configuration",
				"Technician Name",
				"Technician's Summary Report",
				"Remark",
			};

			var fileContent = ExportExcelAccessLogByTeam(VM, columnHeader, "AccessLogByTeam");
			return File(fileContent, "application/ms-excel", "AccessLogReport  By Team.xlsx");
		}

		public byte[] ExportExcelAccessLogByTeam(RegisterViewModel AccessLog, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;
				//using (var cells = worksheet.Cells[4, 4, 4, 8])
				//{
				//	cells.Style.Font.Bold = true;
				//}
				worksheet.Cells[1, 1].Style.Font.Bold = true;
				worksheet.Cells[1, 1].Value = "TeamName : ";
				worksheet.Cells[1, 2].Value = AccessLog.TeamName;

				worksheet.Cells[2, 1].Style.Font.Bold = true;
				worksheet.Cells[2, 1].Value = "From Date :";
				worksheet.Cells[2, 2].Value = string.Format("{0:dd/MMM/yyyy}", AccessLog.fromDate);

				worksheet.Cells[3, 1].Style.Font.Bold = true;
				worksheet.Cells[3, 1].Value = "To Date :";
				worksheet.Cells[3, 2].Value = string.Format("{0:dd/MMM/yyyy}", AccessLog.toDate);

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[5, i + 1].Style.Font.Bold = true;
					worksheet.Cells[5, i + 1].Value = columnHeader[i];
				}
				var j = 6;
				var count = 1;
				foreach (var item in AccessLog.AccessLogList)
				{
					worksheet.Cells["A" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.Date);
					worksheet.Cells["B" + j].Value = item.Device;
					worksheet.Cells["C" + j].Value = item.Reason;
					worksheet.Cells["D" + j].Value = item.TechnicianName;
					worksheet.Cells["E" + j].Value = item.TechnicianReport;
					worksheet.Cells["F" + j].Value = item.Remark;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}
        
		public IActionResult GetReports(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.userList = new SelectList(_db.SysUser.OrderBy(u => u.Name).Distinct().ToList(), "Id", "Name");
			return View(model);
		}

		[HttpPost]
		public IActionResult GetReportsByUserId(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.RegisterList = (from t1 in _db.Register
									join t3 in _db.Device on t1.DeviceId equals t3.Id
									join t4 in _db.SysUser on t1.SysUserId equals t4.Id
									where t4.Id == model.userId
									where t1.Date >= model.fromDate
									where t1.Date <= model.toDate.AddHours(23.50)
									orderby t1.Date ascending
									select new RegisterViewModel
									{
										Id = t1.Id,
										Date = t1.Date,
										Device = t3.Name,
										Reason = t1.Reason,
										TechnicianName = t4.Name,
										TechnicianReport = t1.TechnicianReport,
										Remark = t1.Remark
									}).ToList();
			ViewBag.UserName = _db.SysUser.Where(u => u.Id == model.userId).Select(s => s.Name).FirstOrDefault();
			ViewBag.fromDate = model.fromDate;
			ViewBag.toDate = model.toDate;
			return View(model);
		}

		public IActionResult ExcelExportForReport(Report model)
		{
			RegisterViewModel VM = new RegisterViewModel();
       
                VM.AccessLogList = (from t1 in _db.Register
                                    join t3 in _db.Device on t1.DeviceId equals t3.Id
                                    join t4 in _db.SysUser on t1.SysUserId equals t4.Id
                                    where t4.Id == model.userId
                                    where t1.Date >= model.fromDate
                                    where t1.Date <= model.toDate.AddHours(23.50)
                                    orderby t1.Date
                                    select new RegisterViewModel
                                    {
                                        Id = t1.Id,
                                        Date = t1.Date,
                                        Device = t3.Name,
                                        Reason = t1.Reason,
                                        TechnicianName = t4.Name,
                                        TechnicianReport = t1.TechnicianReport,
                                        Remark = t1.Remark
                                    }
                  ).ToList();
           
			VM.fromDate = model.fromDate;
			VM.toDate = model.toDate;
			VM.StaffName = _db.SysUser.Where(u => u.Id == model.userId).Select( u => u.Name ).FirstOrDefault();
			var columnHeader = new List<string>
			{
				"Date/Time",
				"Equipment Device",
				"Reason of Configuration",
				"Technician Name",
				"Technician's Summary Report",
				"Remark",
			};

			var fileContent = ExportExcelByUserId(VM, columnHeader, "Report");
			return File(fileContent, "application/ms-excel", "Report of IT Equipment Configuration by User.xlsx");
		}

		public byte[] ExportExcelByUserId(RegisterViewModel VM, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;

				worksheet.Cells[1, 1].Style.Font.Bold = true;
				worksheet.Cells[1, 1].Value = "Staff Name : ";
				worksheet.Cells[1, 2].Value = VM.StaffName;

				worksheet.Cells[2, 1].Style.Font.Bold = true;
				worksheet.Cells[2, 1].Value = "From Date :";
				worksheet.Cells[2, 2].Value = string.Format("{0:dd/MMM/yyyy}", VM.fromDate);

				worksheet.Cells[3, 1].Style.Font.Bold = true;
				worksheet.Cells[3, 1].Value = "To Date :";
				worksheet.Cells[3, 2].Value = string.Format("{0:dd/MMM/yyyy}", VM.toDate);

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[5, i + 1].Style.Font.Bold = true;
					worksheet.Cells[5, i + 1].Value = columnHeader[i];
				}
				var j = 6;
				var count = 1;
				foreach (var item in VM.AccessLogList)
				{
					//DateTime? date = item.Date;
					string asString = string.Format("{0:dd/MMM/yyyy}", item.Date);
					worksheet.Cells["A" + j].Value = asString;
					worksheet.Cells["B" + j].Value = item.Device;
					worksheet.Cells["C" + j].Value = item.Reason;
					worksheet.Cells["D" + j].Value = item.TechnicianName;
					worksheet.Cells["E" + j].Value = item.TechnicianReport;
					worksheet.Cells["F" + j].Value = item.Remark;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}
		#endregion

		#region ++ Project : By Date ++
		[HttpGet]
		public IActionResult GetProjectReportsByDate(ProjectReport model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			return View(model);
		}

		[HttpPost]
		public IActionResult GetProjectReports(ProjectReport model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.ProjectReports = (from t1 in _db.RequirementItems
									  join t2 in _db.ProjectData on t1.ProjectId equals t2.Id
									  join t3 in _db.SysUser on t1.CheckTechnicianId equals t3.Id
									  join t4 in _db.SysUser on t1.ImplementTechnicianId equals t4.Id
									  join t5 in _db.Status on t1.StatusId equals t5.Id into gj
									  from subStatus in gj.DefaultIfEmpty()
									  where t1.Date >= model.fromDate
									  where t1.Date <= model.toDate.AddHours(23.50)
									  orderby t1.Date descending
									  //orderby t1.Description descending
									  select new ProjectReport
									  {
										  ProjectDate = t1.Date,
										  RequirementId = t1.Id,
										  RequirementDescription = t1.Description,
										  StartDate = t1.StartDate,
										  EndDate = t1.EndDate,
										  Remark = t1.Remark,
										  ProjectName = t2.Name,
										  StatusName = subStatus.Name ?? String.Empty,
										  ImplementTechnicianName = t4.Name,
										  CheckTechnicianName = t3.Name,
									  }).ToList();
			model.StartDate = model.fromDate;
			model.EndDate = model.toDate;
			return View(model);
		}

		[HttpPost]
		//public IActionResult ExcelExportForProjectReportsByFromDateToDate(ProjectReport model)
		public IActionResult ExcelExportForProjectReportsByFromDateToDate(DateTime FromDate, DateTime ToDate)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ProjectReport model = new ProjectReport();
			model.ProjectReportList = (from t1 in _db.RequirementItems
						join t2 in _db.ProjectData on t1.ProjectId equals t2.Id
						join t3 in _db.SysUser on t1.CheckTechnicianId equals t3.Id
						join t4 in _db.SysUser on t1.ImplementTechnicianId equals t4.Id
						join t5 in _db.Status on t1.StatusId equals t5.Id into gj
						from subStatus in gj.DefaultIfEmpty()
						where t1.Date >= FromDate
						where t1.Date <= ToDate.AddHours(23.50)
						//where t1.Date >= model.fromDate
						//where t1.Date <= model.toDate
						orderby t1.Date descending
						//orderby t1.Description descending
						select new ProjectReport
						{
							ProjectDateForReport = t1.Date,
							RequirementId = t1.Id,
							RequirementDescription = t1.Description,
							StartDate = t1.StartDate,
							EndDate = t1.EndDate,
							Remark = t1.Remark,
							ProjectName = t2.Name,
							StatusName = subStatus.Name ?? String.Empty,
							ImplementTechnicianName = t4.Name,
							CheckTechnicianName = t3.Name,
						}).ToList();
			model.fromDate = FromDate;
			model.toDate = ToDate;

			var columnHeader = new List<string>
			{
				"Date",
				"Requirement Item",
				"Status",
				"Assign/Implement Technician",
				"Assign/Check Technician",
				"Project Name",
				"Remark",
				"Start Date",
				"End Date",
			};

			var fileContent = ExportExcelForProjectReportsByFromDateToDate(model, columnHeader, "ProjectReportsByFromDateToDate");
			return File(fileContent, "application/ms-excel", "Project Reports by FromDate to Date.xlsx");
		}

		public byte[] ExportExcelForProjectReportsByFromDateToDate(ProjectReport model, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;

				worksheet.Cells[1, 1].Style.Font.Bold = true;
				worksheet.Cells[1, 1].Value = "From Date :";
				worksheet.Cells[1, 2].Value = string.Format("{0:dd/MMM/yyyy}", model.fromDate);

				worksheet.Cells[2, 1].Style.Font.Bold = true;
				worksheet.Cells[2, 1].Value = "To Date :";
				worksheet.Cells[2, 2].Value = string.Format("{0:dd/MMM/yyyy}", model.toDate);


				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[4, i + 1].Style.Font.Bold = true;
					worksheet.Cells[4, i + 1].Value = columnHeader[i];
				}
				var j = 5;
				var count = 1;
				foreach (var item in model.ProjectReportList)
				{
					worksheet.Cells["A" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ProjectDateForReport);
					worksheet.Cells["B" + j].Value = item.RequirementDescription;
					worksheet.Cells["C" + j].Value = item.StatusName;
					worksheet.Cells["D" + j].Value = item.ImplementTechnicianName;
					worksheet.Cells["E" + j].Value = item.CheckTechnicianName;
					worksheet.Cells["F" + j].Value = item.ProjectName;
					worksheet.Cells["G" + j].Value = item.Remark;
					worksheet.Cells["H" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.StartDate);
					worksheet.Cells["I" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.EndDate);

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}
		#endregion

		#region ++ Project : By Name ++
		[HttpGet]
		public IActionResult GetReportByProjectName()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.projectList = new SelectList(_db.ProjectData.Distinct().OrderBy(p => p.Name).ToList(), "Id", "Name");
			return View();
		}

		[HttpPost]
		public IActionResult ReportByProjectName(ProjectData model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.ProjectReports = (from t1 in _db.RequirementItems
									  join t2 in _db.ProjectData on t1.ProjectId equals t2.Id
									  join t3 in _db.SysUser on t1.CheckTechnicianId equals t3.Id
									  join t4 in _db.SysUser on t1.ImplementTechnicianId equals t4.Id
									  join t5 in _db.Status on t1.StatusId equals t5.Id into gj
									  from subStatus in gj.DefaultIfEmpty()
									  where t1.ProjectId == model.Id
									  orderby t1.Date descending
									  select new ProjectReport
									  {
										  ProjectDate = t1.Date,
										  RequirementId = t1.Id,
										  RequirementDescription = t1.Description,
										  StartDate = t1.StartDate,
										  EndDate = t1.EndDate,
										  Remark = t1.Remark,
										  ProjectName = t2.Name,
										  StatusName = subStatus.Name ?? String.Empty,
										  ImplementTechnicianName = t4.Name,
										  CheckTechnicianName = t3.Name,
									  }).ToList();
			model.Id = model.Id;
			model.Name = _db.ProjectData.Where(p => p.Id == model.Id).Select(a => a.Name).FirstOrDefault();
			return View(model);
		}

		public IActionResult ExcelExportByProjectName(Guid Id)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ProjectReport model = new ProjectReport();
			model.ProjectReportList = (from t1 in _db.RequirementItems
						join t2 in _db.ProjectData on t1.ProjectId equals t2.Id
						join t3 in _db.SysUser on t1.CheckTechnicianId equals t3.Id
						join t4 in _db.SysUser on t1.ImplementTechnicianId equals t4.Id
						join t5 in _db.Status on t1.StatusId equals t5.Id into gj
						from subStatus in gj.DefaultIfEmpty()
						where t1.ProjectId == Id
						orderby t1.Date descending
						select new ProjectReport
						{
							ProjectDateForReport = t1.Date,
							RequirementId = t1.Id,
							RequirementDescription = t1.Description,
							StartDate = t1.StartDate,
							EndDate = t1.EndDate,
							Remark = t1.Remark,
							ProjectName = t2.Name,
							StatusName = subStatus.Name ?? String.Empty,
							ImplementTechnicianName = t4.Name,
							CheckTechnicianName = t3.Name,
						}).ToList();
			model.ProjectName = _db.ProjectData.Where(u => u.Id == Id).Select(u => u.Name).FirstOrDefault();
			var columnHeader = new List<string>
			{
				"Date",
				"Requirement Item",
				"Project Name",
				"Start Date",
				"End Date",
				"Assign/Implement Technician",
				"Assign/Check Technician",
				"Status",
				"Remark",
			};

			var fileContent = ExportExcelByProjectName(model, columnHeader, "Reports By Project Name");
			return File(fileContent, "application/ms-excel", "Reports By Project Name.xlsx");
		}

		public byte[] ExportExcelByProjectName(ProjectReport model, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;

				worksheet.Cells[1, 1].Style.Font.Bold = true;
				worksheet.Cells[1, 1].Value = "Staff Name : ";
				worksheet.Cells[1, 2].Value = model.ProjectName;


				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[3, i + 1].Style.Font.Bold = true;
					worksheet.Cells[3, i + 1].Value = columnHeader[i];
				}
				var j = 4;
				var count = 1;
				foreach (var item in model.ProjectReportList)
				{
					worksheet.Cells["A" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.ProjectDateForReport);
					worksheet.Cells["B" + j].Value = item.RequirementDescription;
					worksheet.Cells["C" + j].Value = item.ProjectName;
					worksheet.Cells["D" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.StartDate);
					worksheet.Cells["E" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.EndDate);
					worksheet.Cells["F" + j].Value = item.ImplementTechnicianName;
					worksheet.Cells["G" + j].Value = item.CheckTechnicianName;
					worksheet.Cells["H" + j].Value = item.StatusName;
					worksheet.Cells["I" + j].Value = item.Remark;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}
		#endregion

		#region ++ Daily Activity Report ++
		public IActionResult ActivityReportsByTeam(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.TeamList = new SelectList(_db.Team.OrderBy(u => u.Name).Distinct().ToList(), "Id", "Name");
			return View(model);
		}

		[HttpPost]
		public IActionResult GetActivityReportsByTeam(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			//DateTime toDate = model.toDate.AddHours(20.50);
			ViewBag.ActivityList = (from t1 in _db.DailyActivity
									join t4 in _db.SysUser on t1.UserId equals t4.Id
									join t2 in _db.Team on t4.TeamId equals t2.Id
									where t2.Id == model.TeamId
									where t1.CreatedDate >= model.fromDate
									where t1.CreatedDate <= model.toDate.AddHours(23.50)
									//where t1.CreatedDate <= model.toDate.AddDays(1)
									orderby t1.CreatedDate ascending
									select new DailyActivityVM
									{
										Id = t1.Id,
										FinishedTask = t1.FinishedTask,
										ContinueTask = t1.ContinueTask,
										CreatedDate = t1.CreatedDate,
										UserName = t4.Name,
										TeamId = model.TeamId
									}).ToList();
			ViewBag.TeamName = _db.Team.Where(u => u.Id == model.TeamId).Select(s => s.Name).FirstOrDefault();
			ViewBag.fromDate = model.fromDate;
			ViewBag.toDate = model.toDate;
			return View();
		}

		[HttpPost]
		public IActionResult ExportActivityByTeam(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			DailyActivityReport VM = new DailyActivityReport();
			VM.DailyActivityList = (from t1 in _db.DailyActivity
									join t4 in _db.SysUser on t1.UserId equals t4.Id
									join t2 in _db.Team on t4.TeamId equals t2.Id
									where t2.Id == model.TeamId
									where t1.CreatedDate >= model.fromDate
									where t1.CreatedDate <= model.toDate.AddHours(23.50)
									orderby t1.CreatedDate ascending
									select new DailyActivityVM
									{
										Id = t1.Id,
										FinishedTask = t1.FinishedTask,
										ContinueTask = t1.ContinueTask,
										CreatedDate = t1.CreatedDate,
										UserName = t4.Name,
										UserId = t1.UserId,
										TeamId = model.TeamId
									}).ToList();
			VM.TeamName = _db.Team.Where(u => u.Id == model.TeamId).Select(s => s.Name).FirstOrDefault();
			VM.fromDate = model.fromDate;
			VM.toDate = model.toDate;
			var columnHeader = new List<string>
			{
				"Date/Time",
				"Finished Task",
				"Continue Task",
				"Staff Name",
			};

			var fileContent = ExportExcelActivityByTeam(VM, columnHeader, "ActivityByTeam");
			return File(fileContent, "application/ms-excel", "ActivityReport  By Team.xlsx");
		}

		public byte[] ExportExcelActivityByTeam(DailyActivityReport Activity, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;

				worksheet.Cells[1, 1].Style.Font.Bold = true;
				worksheet.Cells[1, 1].Value = "TeamName : ";
				worksheet.Cells[1, 2].Value = Activity.TeamName;

				worksheet.Cells[2, 1].Style.Font.Bold = true;
				worksheet.Cells[2, 1].Value = "From Date :";
				worksheet.Cells[2, 2].Value = string.Format("{0:dd/MMM/yyyy}", Activity.fromDate);

				worksheet.Cells[3, 1].Style.Font.Bold = true;
				worksheet.Cells[3, 1].Value = "To Date :";
				worksheet.Cells[3, 2].Value = string.Format("{0:dd/MMM/yyyy}", Activity.toDate);

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[5, i + 1].Style.Font.Bold = true;
					worksheet.Cells[5, i + 1].Value = columnHeader[i];
				}
				var j = 6;
				var count = 1;
				foreach (var item in Activity.DailyActivityList)
				{
					worksheet.Cells["A" + j].Value = string.Format("{0:dd/MMM/yyyy}", item.CreatedDate);
					worksheet.Cells["B" + j].Value = item.FinishedTask;
					worksheet.Cells["C" + j].Value = item.ContinueTask;
					worksheet.Cells["D" + j].Value = item.UserName;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}

		public IActionResult GetActivityReports(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.userList = new SelectList(_db.SysUser.OrderBy(u => u.Name).Distinct().ToList(), "Id", "Name");
			return View(model);
		}

		[HttpPost]
		public IActionResult GetActivityReportsByUserId(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.ActivityList = (from t1 in _db.DailyActivity
									join t4 in _db.SysUser on t1.UserId equals t4.Id
									where t4.Id == model.userId
									where t1.CreatedDate >= model.fromDate
									where t1.CreatedDate <= model.toDate.AddHours(23.50)
									orderby t1.CreatedDate ascending
									select new DailyActivityVM
									{
										Id = t1.Id,
										FinishedTask = t1.FinishedTask,
										ContinueTask = t1.ContinueTask,
										CreatedDate = t1.CreatedDate,
										//UserId = model.userId
									}).ToList();
			ViewBag.UserName = _db.SysUser.Where(u => u.Id == model.userId).Select(s => s.Name).FirstOrDefault();
			ViewBag.fromDate = model.fromDate;
			ViewBag.toDate = model.toDate;
			return View();
		}

		public IActionResult ExcelExportForActivity(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			DailyActivityVM VM = new DailyActivityVM();
			VM.DailyActivityList = (from t1 in _db.DailyActivity
								join t4 in _db.SysUser on t1.UserId equals t4.Id
								where t4.Id == model.userId
								where t1.CreatedDate >= model.fromDate
								where t1.CreatedDate <= model.toDate.AddHours(23.50)
								orderby t1.CreatedDate ascending
								select new DailyActivity
								{
									Id = t1.Id,
									FinishedTask = t1.FinishedTask,
									ContinueTask = t1.ContinueTask,
									CreatedDate = t1.CreatedDate,
									//UserId = model.userId
								}).ToList();
			VM.fromDate = model.fromDate;
			VM.toDate = model.toDate;
			VM.UserName = _db.SysUser.Where(u => u.Id == model.userId).Select(u => u.Name).FirstOrDefault();
			var columnHeader = new List<string>
			{
				"Date/Time",
				"Finished Task",
				"Continue Task",
			};

			var fileContent = ActivityExportExcelByUserId(VM, columnHeader, "Activity Report");
			return File(fileContent, "application/ms-excel", "Daily Activity by User.xlsx");
		}

		public byte[] ActivityExportExcelByUserId(DailyActivityVM VM, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;

				worksheet.Cells[1, 1].Style.Font.Bold = true;
				worksheet.Cells[1, 1].Value = "Staff Name : ";
				worksheet.Cells[1, 2].Value = VM.UserName;

				worksheet.Cells[2, 1].Style.Font.Bold = true;
				worksheet.Cells[2, 1].Value = "From Date :";
				worksheet.Cells[2, 2].Value = string.Format("{0:dd/MMM/yyyy}", VM.fromDate);

				worksheet.Cells[3, 1].Style.Font.Bold = true;
				worksheet.Cells[3, 1].Value = "To Date :";
				worksheet.Cells[3, 2].Value = string.Format("{0:dd/MMM/yyyy}", VM.toDate);

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[5, i + 1].Style.Font.Bold = true;
					worksheet.Cells[5, i + 1].Value = columnHeader[i];
				}
				var j = 6;
				var count = 1;
				foreach (var item in VM.DailyActivityList)
				{
					string asString = string.Format("{0:dd/MMM/yyyy}", item.CreatedDate);
					worksheet.Cells["A" + j].Value = asString;
					worksheet.Cells["B" + j].Value = item.FinishedTask;
					worksheet.Cells["C" + j].Value = item.ContinueTask;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}

		#endregion

		public IActionResult ExportDailyActivityList(Report model)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			DailyActivityVM VM = new DailyActivityVM();

			VM.DailyActivityList = _db.DailyActivity.Where(d => d.UserId == Guid.Parse(HttpContext.Session.GetString("userId"))).OrderByDescending(l => l.CreatedDate).ToList();
			VM.UserName = _db.SysUser.Where(u => u.Id == Guid.Parse(HttpContext.Session.GetString("userId"))).Select(a => a.Name).FirstOrDefault();

			var columnHeader = new List<string>
			{
				"Date/Time",
				"Finished Task",
				"Continue Task",
			};

			var fileContent = ExportDAList(VM, columnHeader, "Activity Report");
			return File(fileContent, "application/ms-excel", "Daily Activity by User.xlsx");
		}

		public byte[] ExportDAList(DailyActivityVM VM, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;

				worksheet.Cells[1, 1].Style.Font.Bold = true;
				worksheet.Cells[1, 1].Value = "Staff Name : ";
				worksheet.Cells[1, 2].Value = VM.UserName;

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[3, i + 1].Style.Font.Bold = true;
					worksheet.Cells[3, i + 1].Value = columnHeader[i];
				}
				var j = 4;
				var count = 1;
				foreach (var item in VM.DailyActivityList)
				{
					string asString = string.Format("{0:dd/MMM/yyyy}", item.CreatedDate);
					worksheet.Cells["A" + j].Value = asString;
					worksheet.Cells["B" + j].Value = item.FinishedTask;
					worksheet.Cells["C" + j].Value = item.ContinueTask;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}


	}
}