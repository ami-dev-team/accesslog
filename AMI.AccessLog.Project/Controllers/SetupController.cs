﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AMI.AccessLog.Project.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using OfficeOpenXml;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;

namespace AMI.AccessLog.Project.Controllers
{
    public class SetupController : Controller
    {
        AMI_ACCESSLOG_DBContext _db = new AMI_ACCESSLOG_DBContext();
        Common common = new Common();
        public ISession session;

        private IHostingEnvironment _env;

        public SetupController(IHostingEnvironment env)
        {
            _env = env;
        }

        #region ++ Device ++
		void GetViewBagDataForDevice()
		{
			ViewBag.DeviceList = (from t1 in _db.Device
								  join t2 in _db.Location on t1.LocationId equals t2.Id
								  orderby t1.Name
								  select new DeviceViewModel
								  {
									  Id = t1.Id,
									  DeviceName = t1.Name,
									  Ip = t1.Ip,
									  Link = t1.Link,
									  LocationName = t2.Name,
									  Remark = t1.Remark
								  }
			   ).ToList();
			ViewBag.LocationList = new SelectList(_db.Location.OrderBy(l => l.Name).ToList(), "Id", "Name");
		}
		public IActionResult Device(Device device)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			GetViewBagDataForDevice();
			return View(device);
		}

		bool CheckDevice(Device model)
		{
			if (model.Name == null || model.LocationId == Guid.Empty || model.LocationId == null)
			{
				TempData["Message"] = "*Please fill all the required fields";
				return false;
			}
			return true;
		}

		bool CheckForm(String name)
        {
            if (name == null)
            {
                TempData["Message"] = "*Please fill all the required fields";
                return false;
            }
            return true;
        }
        public IActionResult SaveDevice(Device device)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (device.Id == Guid.Empty)
            {
                bool check = CheckDevice(device);
                if (!check)
                {
					GetViewBagDataForDevice();
					return View("Device", device);
                }
                else
                {
                    device.Id = Guid.NewGuid();
					device.CreatedDate = DateTime.Now;
                    _db.Entry(device).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successful.";
                    return RedirectToAction("Device");
                }
            }
            else
            {
				bool check = CheckDevice(device);
				if (ModelState.IsValid && check == true)
                {
                    _db.Update(device);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successful.";
                    return RedirectToAction("Device");
                }
            }
			GetViewBagDataForDevice();
			return View("Device", device);
		}

		public IActionResult DeleteDevice(Guid deviceId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var device = _db.Device.AsNoTracking().SingleOrDefault(m => m.Id == deviceId);
            if (device == null)
            {
				GetViewBagDataForDevice();
				return View("Device");
			}

			try
            {
                var deviceList = _db.Register.Where(u => u.DeviceId == deviceId).ToList();
                if (deviceList.Count == 0)
                {
                    _db.Device.Remove(device);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successful.";
                    return RedirectToAction("Device", new Device());
                }
                else
                {
                    TempData["Message"] = "This device has already used in process.";
                }
				GetViewBagDataForDevice();
				return View("Device", device);
			}
			catch (DbUpdateException)
            {
                return View("Device", new { id = deviceId, saveChangesError = true });
            }
        }

        public IActionResult EditDevice(Guid deviceId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			Device device = _db.Device.Where(r => r.Id == deviceId).FirstOrDefault();
			GetViewBagDataForDevice();
			return View("Device", device);
		}

		public IActionResult ExportDeviceList()
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var device_list = (from t1 in _db.Device
							   join t2 in _db.Location on t1.LocationId equals t2.Id
							   orderby t1.Name
							   select new DeviceViewModel
							   {
								   Id = t1.Id,
								   DeviceName = t1.Name,
								   Ip = t1.Ip,
								   Link = t1.Link,
								   LocationName = t2.Name,
								   Remark = t1.Remark
							   }
						   ).ToList(); ;

            var columnHeader = new List<string>
            {
                "Name",
                "IP",
                "Link",
				"Device Location",
				"Remark",
			};

            var fileContent = ExportExcelForDeviceList(device_list, columnHeader, "Device List");
            return File(fileContent, "application/ms-excel", "DeviceList.xlsx");
        }


        public byte[] ExportExcelForDeviceList(List<DeviceViewModel> device_list, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in device_list)
                {
                    worksheet.Cells["A" + j].Value = item.DeviceName;
                    worksheet.Cells["B" + j].Value = item.Ip;
                    worksheet.Cells["C" + j].Value = item.Link;
					worksheet.Cells["D" + j].Value = item.LocationName;
					worksheet.Cells["E" + j].Value = item.Remark;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }


        #endregion

        public bool CheckLoginUser()
        {
            var userId = HttpContext.Session.GetString("userId");
            if (userId == null)
            {
                return false;
            }
            return true;

        }

        #region ++ Location ++
        public IActionResult Location(Location location)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.LocationList = _db.Location.OrderByDescending(l => l.Name).ToList();
            return View(location);
        }

        public IActionResult SaveLocation(Location location)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (location.Id == Guid.Empty)
            {
                bool check = CheckForm(location.Name);
                if (!check)
                {
					ViewBag.LocationList = _db.Location.OrderByDescending(l => l.Name).ToList();
					return View("Location", location);
                }
                else
                {
                    location.Id = Guid.NewGuid();
					location.CreatedDate = DateTime.Now;
                    _db.Entry(location).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successful.";
                    return RedirectToAction("Location");
                }
            }
            else
            {
                bool check = CheckForm(location.Name);
                if (ModelState.IsValid && check == true)
                {
                    _db.Update(location);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successful.";
                    return RedirectToAction("Location");
                }
            }
			ViewBag.LocationList = _db.Location.OrderByDescending(l => l.Name).ToList();
			return View("Location", location);
		}

		public IActionResult DeleteLocation(Guid locationId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var location = _db.Location.AsNoTracking().SingleOrDefault(m => m.Id == locationId);
            if (location == null)
            {
				ViewBag.LocationList = _db.Location.OrderByDescending(l => l.Name).ToList();
				return View("Location", location);
			}

			try
            {
                var locationList = _db.Device.Where(u => u.LocationId == locationId).ToList();
                if (locationList.Count == 0)
                {
                    _db.Location.Remove(location);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successful.";
                    return RedirectToAction("Location", new Location());
                }
                else
                {
                    TempData["Message"] = "This location has already used in process.";
                }
				ViewBag.LocationList = _db.Location.OrderByDescending(l => l.Name).ToList();
				return View("Location", location);
			}
			catch (DbUpdateException)
            {
                return View("Location", new { id = locationId, saveChangesError = true });
            }
        }

        public IActionResult EditLocation(Guid locationId)
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			Location location = _db.Location.Where(r => r.Id == locationId).FirstOrDefault();
			ViewBag.LocationList = _db.Location.OrderByDescending(l => l.Name).ToList();
			return View("Location", location);
		}

		public IActionResult ExportLocationList()
        {
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var location_list = _db.Location.OrderBy(l => l.Name).ToList();

            var columnHeader = new List<string>
            {
                "Name",
                "Description"
            };

            var fileContent = ExportExcelForLocationList(location_list, columnHeader, "Location List");
            return File(fileContent, "application/ms-excel", "LocationList.xlsx");
        }

        public byte[] ExportExcelForLocationList(List<Location> location_list, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in location_list)
                {
                    worksheet.Cells["A" + j].Value = item.Name;
                    worksheet.Cells["B" + j].Value = item.Description;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }
		#endregion

		#region ++ DailyActivity ++
		public IActionResult DailyActivity(DailyActivity daily)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.DailyActivityList = _db.DailyActivity.Where(d => d.UserId == Guid.Parse(HttpContext.Session.GetString("userId"))).OrderByDescending(l => l.CreatedDate).ToList();
			return View(daily);
		}

		bool CheckFinishedTask(DailyActivity model)
		{
			if (model.CreatedDate == null || model.FinishedTask == null)
			{
				TempData["Message"] = "*Please fill all the required fields";
				ViewBag.DailyActivityList = _db.DailyActivity.Where(d => d.UserId == Guid.Parse(HttpContext.Session.GetString("userId"))).OrderByDescending(l => l.CreatedDate).ToList();
				return false;
			}
			return true;
		}

		public IActionResult SaveDailyActivity(DailyActivity daily)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (daily.Id == Guid.Empty)
			{
				if (!CheckFinishedTask(daily))
					return View("DailyActivity", daily);
				
					daily.Id = Guid.NewGuid();
					daily.CreatedDate = daily.CreatedDate;
					daily.UserId = Guid.Parse(HttpContext.Session.GetString("userId"));
					_db.Entry(daily).State = EntityState.Added;
					_db.SaveChanges();
					TempData["Message"] = "Save Successful.";
					return RedirectToAction("DailyActivity");
			}
			else
			{
				if (!CheckFinishedTask(daily))
					return View("DailyActivity", daily);

				if (ModelState.IsValid)
				{
					daily.CreatedDate = daily.CreatedDate;
					daily.UserId = Guid.Parse(HttpContext.Session.GetString("userId"));
					_db.Update(daily);
					_db.SaveChanges();
					TempData["Message"] = "Update Successful.";
					return RedirectToAction("DailyActivity");
				}
			}
			ViewBag.DailyActivityList = _db.DailyActivity.Where(d => d.UserId == Guid.Parse(HttpContext.Session.GetString("userId"))).OrderByDescending(l => l.CreatedDate).ToList();
			return View("DailyActivity", daily);
		}

		public IActionResult DeleteActivity(Guid Id)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var Activity = _db.DailyActivity.AsNoTracking().SingleOrDefault(m => m.Id == Id);
			_db.DailyActivity.Remove(Activity);
			_db.SaveChanges();
			TempData["Message"] = "Delete Successful.";
			return RedirectToAction("DailyActivity");
		}

		public IActionResult EditActivity(Guid Id)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			DailyActivity Activity = _db.DailyActivity.Where(r => r.Id == Id).FirstOrDefault();
			ViewBag.DailyActivityList = _db.DailyActivity.Where(d => d.UserId == Guid.Parse(HttpContext.Session.GetString("userId"))).OrderByDescending(l => l.CreatedDate).ToList();
			return View("DailyActivity", Activity);
		}
		#endregion

		#region ++ Team ++
		public IActionResult Team(Team Team)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			ViewBag.TeamList = _db.Team.OrderByDescending(p => p.Name).ToList();
			return View(Team);
		}

		bool CheckTeamForm(Team model)
		{
			if (model.Name == null)
			{
				TempData["Message"] = "*Please fill all the required fields";
				return false;
			}
			return true;
		}
		public IActionResult SaveTeam(Team Team)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			if (Team.Id == Guid.Empty)
			{
				bool check = CheckTeamForm(Team);
				if (!check)
				{
					ViewBag.TeamList = _db.Team.OrderByDescending(p => p.Name).ToList();
					return View("Team", Team);
				}
				else
				{
					Team.Id = Guid.NewGuid();
					Team.CreatedDate = DateTime.Now;
					_db.Entry(Team).State = EntityState.Added;
					_db.SaveChanges();
					TempData["Message"] = "Save Successful.";
					return RedirectToAction("Team");
				}
			}
			else
			{
				bool check = CheckTeamForm(Team);
				if (ModelState.IsValid && check == true)
				{
					_db.Update(Team);
					_db.SaveChanges();
					TempData["Message"] = "Update Successful.";
					return RedirectToAction("Team");
				}
			}
			ViewBag.TeamList = _db.Team.OrderByDescending(p => p.Name).ToList();
			return View("Team", Team);
		}

		public IActionResult EditTeam(Guid Id)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			Team Team = _db.Team.Where(p => p.Id == Id).FirstOrDefault();
			ViewBag.TeamList = _db.Team.OrderByDescending(p => p.Name).ToList();
			return View("Team", Team);
		}

		public IActionResult DeleteTeam(Guid Id)
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var Team = _db.Team.AsNoTracking().SingleOrDefault(m => m.Id == Id);
			if (Team == null)
			{
				ViewBag.TeamList = _db.Team.OrderByDescending(p => p.Name).ToList();
				return View("Team", Team);
			}

			try
			{
				var userList = _db.SysUser.Where(u => u.TeamId == Id).ToList();
				var ProjectList = _db.ProjectData.Where(p => p.TeamId == Id).ToList();
				if (userList.Count == 0 && ProjectList.Count == 0)
				{
					_db.Team.Remove(Team);
					_db.SaveChanges();
					TempData["Message"] = "Delete Successful.";
					ViewBag.TeamList = _db.Team.ToList();
					return RedirectToAction("Team");
				}
				else
				{
					TempData["Message"] = "This Team has already used in process.";
				}
				ViewBag.TeamList = _db.Team.OrderByDescending(p => p.Name).ToList();
				return View("Team", Team);
			}
			catch (DbUpdateException)
			{
				return View("Team", new { id = Id, saveChangesError = true });
			}
		}


		public IActionResult ExportTeamList()
		{
			if (!CheckLoginUser())
				return RedirectToAction("Login", "Account");

			var TeamList = _db.Team.OrderBy(p => p.Name).ToList();

			var columnHeader = new List<string>
			{
				"Name",
				"Description"
			};

			var fileContent = ExportExcelForTeamList(TeamList, columnHeader, "Team List");
			return File(fileContent, "application/ms-excel", "TeamList.xlsx");
		}


		public byte[] ExportExcelForTeamList(List<Team> TeamList, List<string> columnHeader, string heading)
		{
			byte[] result = null;
			using (ExcelPackage package = new ExcelPackage())
			{
				var worksheet = package.Workbook.Worksheets.Add(heading);
				worksheet.DefaultColWidth = 20;
				using (var cells = worksheet.Cells[1, 1, 1, 8])
				{
					cells.Style.Font.Bold = true;
				}

				for (int i = 0; i < columnHeader.Count(); i++)
				{
					worksheet.Cells[1, i + 1].Value = columnHeader[i];
				}
				var j = 2;
				var count = 1;
				foreach (var item in TeamList)
				{
					worksheet.Cells["A" + j].Value = item.Name;
					worksheet.Cells["B" + j].Value = item.Description;

					j++;
					count++;
				}
				result = package.GetAsByteArray();
			}
			return result;
		}

        #endregion++ Team ++

        #region ++ Branch ++

        public IActionResult Branch()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ViewBag.BranchList = _db.Branch.OrderBy(b => b.BranchCode).ToList();
            return View(new Branch());
        }

        [HttpPost]
        public IActionResult Branch(Branch model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            try
            {
                if (model.Id == Guid.Empty)
                {
                    model.Id = Guid.NewGuid();
                    model.BranchCode = GetBranchCode();
                    model.CreatedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;

                    _db.Entry(model).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successfully!";
                    return RedirectToAction("Branch");
                }
                else
                {
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedDate = DateTime.Now;
                    _db.Update(model);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successfully!";
                    return RedirectToAction("Branch");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("Branch");
            }
        }

        public string GetBranchCode()
        {
            string code = "B";
            int serialNo = 4;
            List<Branch> branches = new List<Branch>();
            branches = _db.Branch.ToList();

            if (branches.Count() == 0)
            {
                code = code + "0001";
            }
            else
            {
                string Branchserial = "";
                int rowCount = branches.Count;
                int serialZero = serialNo - rowCount.ToString().Length;
                for (int i = 1; i <= serialZero; i++)
                {
                    Branchserial += "0";
                }
                code = code + Branchserial + Convert.ToInt32(branches.Count() + 1);
                var branchCode = _db.Branch.Where(x => x.BranchCode == code).ToList();
                if (branchCode.Count() == 1)
                {
                    code = "B";
                    code = code + Branchserial + Convert.ToInt32(branches.Count() + 2);
                }
            }
            return code;
        }

        public IActionResult ExportBranchList()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var BranchList = _db.Branch.OrderBy(l => l.BranchCode).ToList();

            var columnHeader = new List<string>
            {
                "BranchCode",
                "BranchName",
                "Address",
                "Active"
            };

            var fileContent = ExportExcelForBranchList(BranchList, columnHeader, "Branch List");
            return File(fileContent, "application/ms-excel", "BranchList.xlsx");
        }

        public byte[] ExportExcelForBranchList(List<Branch> BranchList, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in BranchList)
                {
                    worksheet.Cells["A" + j].Value = item.BranchCode;
                    worksheet.Cells["B" + j].Value = item.BranchName;
                    worksheet.Cells["C" + j].Value = item.Address;
                    worksheet.Cells["D" + j].Value = item.Active;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ImportBranchList()
        {
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        [Route("ImportBranchDataList")]
        public IActionResult ImportBranchDataList(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                try
                {
                    string folderName = "ImportedFiles";
                    string webRootPath = _env.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories

                    if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                    using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                    {
                        reportfile.CopyTo(fileStream);
                    }
                    // Get uploaded file path with root
                    string rootFolder = _env.WebRootPath;
                    string fileName = @"ImportedFiles/" + fiName;
                    FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                        int totalRows = workSheet.Dimension.Rows;
                        List<Branch> BranchList = new List<Branch>();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            Branch branch = new Branch();
                            branch.Id = Guid.NewGuid();
                            branch.BranchCode = workSheet.Cells[i, 1].Value.ToString();
                            branch.BranchName = workSheet.Cells[i, 2].Value.ToString();
                            if (workSheet.Cells[i, 3].Value == null)
                            {
                                branch.Address = "";
                            }
                            else
                            {
                                branch.Address = workSheet.Cells[i, 3].Value.ToString();
                            }

                            branch.Active = true;
                            if (workSheet.Cells[i, 4].Value.ToString() == "TRUE")
                            {
                                branch.Active = true;
                            }
                            else if (workSheet.Cells[i, 4].Value.ToString() == "FALSE")
                            {
                                branch.Active = false;
                            }

                            branch.CreatedBy = HttpContext.Session.GetString("userId");
                            branch.CreatedDate = DateTime.Now;
                            branch.ModifiedBy = HttpContext.Session.GetString("userId");
                            branch.ModifiedDate = DateTime.Now;

                            BranchList.Add(branch);
                        }
                        _db.Branch.AddRange(BranchList);
                        _db.SaveChanges();
                        TempData["Message"] = "Import Data Successfully.";
                        return RedirectToAction("Branch");
                    }
                }
                catch (Exception ex)
                {
                    TempData["Message"] = ex.Message.ToString();
                    return RedirectToAction("Branch");
                }
            }

        }

        public IActionResult EditBranch(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            Branch branch = new Branch();
            branch = _db.Branch.Where(r => r.Id == Id).FirstOrDefault();
            ViewBag.BranchList = _db.Branch.OrderBy(b => b.BranchCode).ToList();
            return View("Branch", branch);
        }

        public IActionResult DeleteBranch(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            //var branch = _db.Branch.AsNoTracking().SingleOrDefault(m => m.Id == Id);
            //if (branch == null)
            //{
            //    return RedirectToAction(nameof(Branch));
            //}

            try
            {
                Branch branch = _db.Branch.Where(x => x.Id == Id).FirstOrDefault();
                if (branch != null)
                {
                    _db.Branch.Remove(branch);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successfully.";
                    return RedirectToAction("Branch");
                }
                //else
                //{
                //    TempData["Message"] = "This Branch has already used in process.";
                //    return RedirectToAction("Branch");
                //}
                return RedirectToAction("Branch");
            }
            catch (DbUpdateException ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("Branch");
            }
        }
        #endregion

        #region ++ Department ++
        public IActionResult Department(Department model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ViewBag.DepartmentList = _db.Department.OrderBy(d => d.DepartmentCode).ToList();
            return View(model);
        }

        public IActionResult SaveDepartment(Department model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            try
            {
                if (model.Id == Guid.Empty)
                {
                    model.Id = Guid.NewGuid();
                    model.DepartmentCode = GetDeptCode();
                    model.CreatedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;

                    _db.Entry(model).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successfully!";
                    return RedirectToAction("Department");
                }
                else
                {
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedDate = DateTime.Now;
                    _db.Update(model);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successfully!";
                    return RedirectToAction("Department");
                }
            }
            catch(Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("Department");
            }
        }

        public string GetDeptCode()
        {
            string code = "D";
            int serialNo = 4;
            List<Department> departments = new List<Department>();
            departments = _db.Department.ToList();

            if (departments.Count() == 0)
            {
                code = code + "0001";
            }
            else
            {
                string serial = "";
                int rowCount = departments.Count;
                int serialZero = serialNo - rowCount.ToString().Length;
                for (int i = 1; i <= serialZero; i++)
                {
                    serial += "0";
                }
                code = code + serial + Convert.ToInt32(departments.Count() + 1);
                var deptCode = _db.Department.Where(x => x.DepartmentCode == code).ToList();
                if (deptCode.Count() == 1)
                {
                    code = "D";
                    code = code + serial + Convert.ToInt32(departments.Count() + 2);
                }
            }
            return code;
        }

        public IActionResult DeleteDepartment(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var dept = _db.Department.AsNoTracking().SingleOrDefault(m => m.Id == Id);
            if (dept == null)
            {
                ViewBag.DepartmentList = _db.Department.OrderBy(l => l.DepartmentCode).ToList();
                return View("Department", dept);
            }

            try
            {
                Department department = _db.Department.Where(u => u.Id == Id).FirstOrDefault();
                if (department !=null)
                {
                    _db.Department.Remove(department);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successful.";
                    return RedirectToAction("Department", new Department());
                }
                //else
                //{
                //    TempData["Message"] = "This location has already used in process.";
                //}
                return RedirectToAction("Department", new Department());
            }
            catch (DbUpdateException)
            {
                return View("Department", new { id = Id, saveChangesError = true });
            }
        }

        public IActionResult EditDepartment(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            Department dept = new Department();
            dept = _db.Department.Where(r => r.Id == Id).FirstOrDefault();
            ViewBag.DepartmentList = _db.Department.OrderBy(b => b.DepartmentCode).ToList();
            return View("Department", dept);
        }

        public IActionResult ExportDepartmentList()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var dept_list = _db.Department.OrderBy(l => l.DepartmentCode).ToList();

            var columnHeader = new List<string>
            {
                "DepartmentCode",
                "DepartmentName"
            };

            var fileContent = ExportExcelForLocationList(dept_list, columnHeader, "Department List");
            return File(fileContent, "application/ms-excel", "DepartmentList.xlsx");
        }

        public byte[] ExportExcelForLocationList(List<Department> dept_list, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in dept_list)
                {
                    worksheet.Cells["A" + j].Value = item.DepartmentCode;
                    worksheet.Cells["B" + j].Value = item.DepartmentName;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ImportDepartmentList()
        {
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        [Route("ImportDepartmentDataList")]
        public IActionResult ImportDepartmentDataList(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                try
                {
                    string folderName = "ImportedFiles";
                    string webRootPath = _env.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories

                    if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                    using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                    {
                        reportfile.CopyTo(fileStream);
                    }
                    // Get uploaded file path with root
                    string rootFolder = _env.WebRootPath;
                    string fileName = @"ImportedFiles/" + fiName;
                    FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                        int totalRows = workSheet.Dimension.Rows;
                        List<Department> DeptList = new List<Department>();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            Department dept = new Department();
                            dept.Id = Guid.NewGuid();
                            dept.DepartmentCode = workSheet.Cells[i, 1].Value.ToString();
                            dept.DepartmentName = workSheet.Cells[i, 2].Value.ToString();
                          
                            dept.CreatedBy = HttpContext.Session.GetString("userId");
                            dept.CreatedDate = DateTime.Now;
                            dept.ModifiedBy = HttpContext.Session.GetString("userId");
                            dept.ModifiedDate = DateTime.Now;

                            DeptList.Add(dept);
                        }
                        _db.Department.AddRange(DeptList);
                        _db.SaveChanges();
                        TempData["Message"] = "Import Data Successfully.";
                        return RedirectToAction("Department");
                    }
                }
                catch (Exception ex)
                {
                    TempData["Message"] = ex.Message.ToString();
                    return RedirectToAction("Department");
                }
            }

        }

        #endregion

        #region ++ ServicePerson ++

        public IActionResult ServicePerson(ServicePerson model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            ViewBag.PositionList = new SelectList(_db.Position.OrderBy(r => r.Name).ToList(), "Id", "Name");

            ViewBag.ServicePersonList = (from t1 in _db.ServicePerson
                                         join t2 in _db.Position on t1.Position equals t2.Id
                                         orderby t2.Name
                                         select new ServicePersonInfo
                                         {
                                             Id = t1.Id,
                                             PersonId = t1.PersonId,
                                             PersonName = t1.PersonName,
                                             PositionName = t2.Name,
                                             Active = t1.Active
                                         }).ToList();

            return View(model);
        }

        public IActionResult SaveServicePerson(ServicePerson model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            try
            {
                if (model.Id == Guid.Empty)
                {
                    model.Id = Guid.NewGuid();
                    model.CreatedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;

                    _db.Entry(model).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successfully!";
                    return RedirectToAction("ServicePerson");
                }
                else
                {
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedDate = DateTime.Now;
                    _db.Update(model);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successfully!";
                    return RedirectToAction("ServicePerson");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("ServicePerson");
            }
        }

        public IActionResult DeleteServicePerson(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var sp = _db.ServicePerson.AsNoTracking().SingleOrDefault(m => m.Id == Id);
            if (sp == null)
            {
                return View("ServicePerson", sp);
            }

            try
            {
                ServicePerson sperson = _db.ServicePerson.Where(u => u.Id == Id).FirstOrDefault();
                if (sperson != null)
                {
                    _db.ServicePerson.Remove(sperson);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successful.";
                    return RedirectToAction("ServicePerson", new ServicePerson());
                }
                //else
                //{
                //    TempData["Message"] = "This location has already used in process.";
                //}
                return RedirectToAction("ServicePerson", new ServicePerson());
            }
            catch (DbUpdateException)
            {
                return View("ServicePerson", new { id = Id, saveChangesError = true });
            }
        }

        public IActionResult EditServicePerson(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ServicePerson sp = new ServicePerson();
            sp = _db.ServicePerson.Where(x => x.Id == Id).FirstOrDefault();

            ViewBag.PositionList = new SelectList(_db.Position.OrderBy(r => r.Name).ToList(), "Id", "Name");
            ViewBag.ServicePersonList = (from t1 in _db.ServicePerson
                                         join t2 in _db.Position on t1.Position equals t2.Id
                                         orderby t2.Name
                                         select new ServicePersonInfo
                                         {
                                             Id = t1.Id,
                                             PersonId = t1.PersonId,
                                             PersonName = t1.PersonName,
                                             PositionName = t2.Name,
                                             Active = t1.Active
                                         }).ToList();

            return View("ServicePerson", sp);
        }

        public IActionResult ExportPersonList()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            //var sp_list = _db.ServicePerson.OrderBy(l => l.PersonName).ToList();
            var sp_list = (from t1 in _db.ServicePerson
                           join t2 in _db.Position on t1.Position equals t2.Id
                           orderby t2.Name
                           select new ServicePersonInfo
                           {
                               Id = t1.Id,
                               PersonId = t1.PersonId,
                               PersonName = t1.PersonName,
                               PositionName = t2.Name,
                               Active = t1.Active
                           }).ToList();
            var columnHeader = new List<string>
            {
                "PersonId",
                "PersonName",
                "Position",
                "Active"
            };

            var fileContent = ExportExcelForPersonList(sp_list, columnHeader, "Service Person List");
            return File(fileContent, "application/ms-excel", "ServicePersonList.xlsx");
        }

        public byte[] ExportExcelForPersonList(List<ServicePersonInfo> sp_list, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in sp_list)
                {
                    worksheet.Cells["A" + j].Value = item.PersonId;
                    worksheet.Cells["B" + j].Value = item.PersonName;
                    worksheet.Cells["C" + j].Value = item.PositionName;
                    worksheet.Cells["D" + j].Value = item.Active;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ImportServicePersonList()
        {
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        [Route("ImportServicePersonDataList")]
        public IActionResult ImportServicePersonDataList(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                try
                {
                    string folderName = "ImportedFiles";
                    string webRootPath = _env.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories

                    if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                    using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                    {
                        reportfile.CopyTo(fileStream);
                    }
                    // Get uploaded file path with root
                    string rootFolder = _env.WebRootPath;
                    string fileName = @"ImportedFiles/" + fiName;
                    FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                        int totalRows = workSheet.Dimension.Rows;
                        List<ServicePerson> SPList = new List<ServicePerson>();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            ServicePerson sp = new ServicePerson();
                            sp.Id = Guid.NewGuid();
                            sp.PersonId = workSheet.Cells[i, 1].Value.ToString();
                            sp.PersonName = workSheet.Cells[i, 2].Value.ToString();
                            string positionName = workSheet.Cells[i, 3].Value.ToString();

                            Guid positionId = (from x in _db.Position
                                               where x.Name == positionName
                                               select x.Id).FirstOrDefault();
                            sp.Position = positionId;

                            sp.Active = true;
                            if (workSheet.Cells[i, 4].Value.ToString() == "TRUE")
                            {
                                sp.Active = true;
                            }
                            else if (workSheet.Cells[i, 4].Value.ToString() == "FALSE")
                            {
                                sp.Active = false;
                            }
                            sp.CreatedBy = HttpContext.Session.GetString("userId");
                            sp.ModifiedBy = HttpContext.Session.GetString("userId");
                            sp.CreatedDate = DateTime.Now;
                            sp.ModifiedDate = DateTime.Now;

                            SPList.Add(sp);
                        }
                        _db.ServicePerson.AddRange(SPList);
                        _db.SaveChanges();
                        TempData["Message"] = "Import Data Successfully.";
                        return RedirectToAction("ServicePerson");
                    }
                }
                catch (Exception ex)
                {
                    TempData["Message"] = ex.Message.ToString();
                    return RedirectToAction("ServicePerson");
                }
            }

        }

        #endregion

        #region ++ Channel ++
        public IActionResult Channel(Channel model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ViewBag.ChannelList = _db.Channel.OrderBy(d => d.ChannelCode).ToList();
            return View(model);
        }

        public IActionResult SaveChannel(Channel model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            try
            {
                if (model.Id == Guid.Empty)
                {
                    model.Id = Guid.NewGuid();
                    model.ChannelCode = GetChannelCode();
                    model.CreatedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;

                    _db.Entry(model).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successfully!";
                    return RedirectToAction("Channel");
                }
                else
                {
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedDate = DateTime.Now;
                    _db.Update(model);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successfully!";
                    return RedirectToAction("Channel");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("Channel");
            }
        }

        public string GetChannelCode()
        {
            string code = "C";
            int serialNo = 4;
            List<Channel> channels = new List<Channel>();
            channels = _db.Channel.ToList();

            if (channels.Count() == 0)
            {
                code = code + "0001";
            }
            else
            {
                string serial = "";
                int rowCount = channels.Count;
                int serialZero = serialNo - rowCount.ToString().Length;
                for (int i = 1; i <= serialZero; i++)
                {
                    serial += "0";
                }
                code = code + serial + Convert.ToInt32(channels.Count() + 1);
                var channelCode = _db.Channel.Where(x => x.ChannelCode == code).ToList();
                if (channelCode.Count() == 1)
                {
                    code = "C";
                    code = code + serial + Convert.ToInt32(channels.Count() + 2);
                }
            }
            return code;
        }

        public IActionResult DeleteChannel(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var dept = _db.Channel.AsNoTracking().SingleOrDefault(m => m.Id == Id);
            if (dept == null)
            {
                ViewBag.ChannelList = _db.Channel.OrderBy(l => l.ChannelCode).ToList();
                return View("Channel", dept);
            }

            try
            {
                Channel channel = _db.Channel.Where(u => u.Id == Id).FirstOrDefault();
                if (channel != null)
                {
                    _db.Channel.Remove(channel);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successful.";
                    return RedirectToAction("Channel", new Channel());
                }
                //else
                //{
                //    TempData["Message"] = "This location has already used in process.";
                //}
                return RedirectToAction("Channel", new Channel());
            }
            catch (DbUpdateException)
            {
                return View("Channel", new { id = Id, saveChangesError = true });
            }
        }

        public IActionResult EditChannel(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            Channel chn = new Channel();
            chn = _db.Channel.Where(r => r.Id == Id).FirstOrDefault();
            ViewBag.ChannelList = _db.Channel.OrderBy(b => b.ChannelCode).ToList();
            return View("Channel", chn);
        }

        public IActionResult ExportChannelList()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var chn_list = _db.Channel.OrderBy(l => l.ChannelCode).ToList();

            var columnHeader = new List<string>
            {
                "ChannelCode",
                "ChannelName"
            };

            var fileContent = ExportExcelForChannelList(chn_list, columnHeader, "Channel List");
            return File(fileContent, "application/ms-excel", "ChannelList.xlsx");
        }

        public byte[] ExportExcelForChannelList(List<Channel> chn_list, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in chn_list)
                {
                    worksheet.Cells["A" + j].Value = item.ChannelCode;
                    worksheet.Cells["B" + j].Value = item.ChannelName;

                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ImportChannelList()
        {
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        [Route("ImportChannelDataList")]
        public IActionResult ImportChannelDataList(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                try
                {
                    string folderName = "ImportedFiles";
                    string webRootPath = _env.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories

                    if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                    using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                    {
                        reportfile.CopyTo(fileStream);
                    }
                    // Get uploaded file path with root
                    string rootFolder = _env.WebRootPath;
                    string fileName = @"ImportedFiles/" + fiName;
                    FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                        int totalRows = workSheet.Dimension.Rows;
                        List<Channel> ChnList = new List<Channel>();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            Channel chn = new Channel();
                            chn.Id = Guid.NewGuid();
                            chn.ChannelCode = workSheet.Cells[i, 1].Value.ToString();
                            chn.ChannelName = workSheet.Cells[i, 2].Value.ToString();

                            chn.CreatedBy = HttpContext.Session.GetString("userId");
                            chn.CreatedDate = DateTime.Now;
                            chn.ModifiedBy = HttpContext.Session.GetString("userId");
                            chn.ModifiedDate = DateTime.Now;

                            ChnList.Add(chn);
                        }
                        _db.Channel.AddRange(ChnList);
                        _db.SaveChanges();
                        TempData["Message"] = "Import Data Successfully.";
                        return RedirectToAction("Channel");
                    }
                }
                catch (Exception ex)
                {
                    TempData["Message"] = ex.Message.ToString();
                    return RedirectToAction("Channel");
                }
            }

        }

        #endregion

        #region ++ Issue Type ++
        public IActionResult IssueType()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            ViewBag.IssueTypeList = _db.IssueType.OrderBy(I => I.IssueTypeCode).ToList();

            return View(new IssueType());
        }

        [HttpPost]
        public IActionResult IssueType(IssueType model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            try
            {
                if (model.Id == Guid.Empty)
                {
                    model.Id = Guid.NewGuid();
                    model.IssueTypeCode = GetIssueTypeCode();
                    model.CreatedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;

                    _db.Entry(model).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successfully!";
                    return RedirectToAction("IssueType");
                }
                else
                {
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedDate = DateTime.Now;
                    _db.Update(model);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successfully!";
                    return RedirectToAction("IssueType");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("IssueType");
            }
        }

        public string GetIssueTypeCode()
        {
            string code = "I";
            int serialNo = 4;
            List<IssueType> issueTypes = new List<IssueType>();
            issueTypes = _db.IssueType.ToList();

            if (issueTypes.Count() == 0)
            {
                code = code + "0001";
            }
            else
            {
                string IssueTypeserial = "";
                int rowCount = issueTypes.Count;
                int serialZero = serialNo - rowCount.ToString().Length;
                for (int i = 1; i <= serialZero; i++)
                {
                    IssueTypeserial += "0";
                }
                code = code + IssueTypeserial + Convert.ToInt32(issueTypes.Count() + 1);
                var issueTypeCode = _db.IssueType.Where(x => x.IssueTypeCode == code).ToList();
                if (issueTypeCode.Count() == 1)
                {
                    code = "I";
                    code = code + IssueTypeserial + Convert.ToInt32(issueTypes.Count() + 2);
                }
            }
            return code;
        }

        public IActionResult EditIssueType(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            IssueType issueType = new IssueType();
            issueType = _db.IssueType.Where(r => r.Id == Id).FirstOrDefault();
            ViewBag.IssueTypeList = _db.IssueType.OrderBy(b => b.IssueTypeCode).ToList();
            return View("IssueType", issueType);
        }

        public IActionResult DeleteIssueType(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var issueType = _db.IssueType.AsNoTracking().SingleOrDefault(m => m.Id == Id);
            if (issueType == null)
            {
                return RedirectToAction(nameof(IssueType));
            }

            try
            {
                ServiceData serviceData = _db.ServiceData.Where(x => x.Id == Id).FirstOrDefault();
                if (serviceData == null)
                {
                    _db.IssueType.Remove(issueType);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successfully.";
                    return RedirectToAction("IssueType");
                }
                else
                {
                    TempData["Message"] = "This IssueType has already used in process.";
                    return RedirectToAction("IssueType");
                }
                //return RedirectToAction("Branch");
            }
            catch (DbUpdateException ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("IssueType");
            }
        }

        public IActionResult ExportIssueTypeList()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var IssueTypeList = _db.IssueType.OrderBy(l => l.IssueTypeCode).ToList();

            var columnHeader = new List<string>
            {
                "IssueType Code",
                "IssueType Name"
            };

            var fileContent = ExportExcelForIssueTypeList(IssueTypeList, columnHeader, "IssueType List");
            return File(fileContent, "application/ms-excel", "IssueTypeList.xlsx");
        }

        public byte[] ExportExcelForIssueTypeList(List<IssueType> IssueTypeList, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in IssueTypeList)
                {
                    worksheet.Cells["A" + j].Value = item.IssueTypeCode;
                    worksheet.Cells["B" + j].Value = item.IssueTypeName;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ImportIssueTypeList()
        {
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public IActionResult ImportIssueTypeList(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                try
                {
                    string folderName = "ImportedFiles";
                    string webRootPath = _env.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories

                    if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                    using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                    {
                        reportfile.CopyTo(fileStream);
                    }
                    // Get uploaded file path with root
                    string rootFolder = _env.WebRootPath;
                    string fileName = @"ImportedFiles/" + fiName;
                    FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                        int totalRows = workSheet.Dimension.Rows;
                        List<IssueType> IssueTypeList = new List<IssueType>();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            IssueType issueType = new IssueType();
                            issueType.Id = Guid.NewGuid();
                            issueType.IssueTypeCode = workSheet.Cells[i, 1].Value.ToString();
                            issueType.IssueTypeName = workSheet.Cells[i, 2].Value.ToString();

                            issueType.CreatedBy = HttpContext.Session.GetString("userId");
                            issueType.CreatedDate = DateTime.Now;
                            issueType.ModifiedBy = HttpContext.Session.GetString("userId");
                            issueType.ModifiedDate = DateTime.Now;
                            IssueTypeList.Add(issueType);
                        }
                        
                        _db.IssueType.AddRange(IssueTypeList);
                        _db.SaveChanges();
                        TempData["Message"] = "Import Data Successfully.";
                        return RedirectToAction("IssueType");
                    }
                }
                catch (Exception ex)
                {
                    TempData["Message"] = ex.Message.ToString();
                    return RedirectToAction("IssueType");
                }
            }

        }

        #endregion

        #region ++ Status ++
        public IActionResult ServiceStatus()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");
            ViewBag.ServiceStatusList = _db.ServiceStatus.OrderBy(I => I.StatusCode).ToList();

            return View(new ServiceStatus());
        }

        [HttpPost]
        public IActionResult ServiceStatus(ServiceStatus model)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            try
            {
                if (model.Id == Guid.Empty)
                {
                    model.Id = Guid.NewGuid();
                    model.StatusCode = GetStatusCode();
                    model.CreatedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;

                    _db.Entry(model).State = EntityState.Added;
                    _db.SaveChanges();
                    TempData["Message"] = "Save Successfully!";
                    return RedirectToAction("ServiceStatus");
                }
                else
                {
                    model.ModifiedBy = HttpContext.Session.GetString("userId");
                    model.ModifiedDate = DateTime.Now;
                    _db.Update(model);
                    _db.SaveChanges();
                    TempData["Message"] = "Update Successfully!";
                    return RedirectToAction("ServiceStatus");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("ServiceStatus");
            }
        }

        public string GetStatusCode()
        {
            string code = "S";
            int serialNo = 4;
            List<ServiceStatus> serviceStatuses = new List<ServiceStatus>();
            serviceStatuses = _db.ServiceStatus.ToList();

            if (serviceStatuses.Count() == 0)
            {
                code = code + "0001";
            }
            else
            {
                string ServiceStatusserial = "";
                int rowCount = serviceStatuses.Count;
                int serialZero = serialNo - rowCount.ToString().Length;
                for (int i = 1; i <= serialZero; i++)
                {
                    ServiceStatusserial += "0";
                }
                code = code + ServiceStatusserial + Convert.ToInt32(serviceStatuses.Count() + 1);
                var issueTypeCode = _db.ServiceStatus.Where(x => x.StatusCode == code).ToList();
                if (issueTypeCode.Count() == 1)
                {
                    code = "S";
                    code = code + ServiceStatusserial + Convert.ToInt32(serviceStatuses.Count() + 2);
                }
            }
            return code;
        }

        public IActionResult EditServiceStatus(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus = _db.ServiceStatus.Where(r => r.Id == Id).FirstOrDefault();
            ViewBag.serviceStatusList = _db.ServiceStatus.OrderBy(b => b.StatusCode).ToList();
            return View("ServiceStatus", serviceStatus);
        }

        public IActionResult DeleteServiceStatus(Guid Id)
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var serviceStatus = _db.ServiceStatus.AsNoTracking().SingleOrDefault(m => m.Id == Id);
            if (serviceStatus == null)
            {
                return RedirectToAction(nameof(ServiceStatus));
            }

            try
            {
                ServiceData serviceData = _db.ServiceData.Where(x => x.Id == Id).FirstOrDefault();
                if (serviceData == null)
                {
                    _db.ServiceStatus.Remove(serviceStatus);
                    _db.SaveChanges();
                    TempData["Message"] = "Delete Successfully.";
                    return RedirectToAction("ServiceStatus");
                }
                else
                {
                    TempData["Message"] = "This Code has already used in process.";
                    return RedirectToAction("ServiceStatus");
                }
                //return RedirectToAction("Branch");
            }
            catch (DbUpdateException ex)
            {
                TempData["Message"] = ex.Message.ToString();
                return RedirectToAction("ServiceStatus");
            }
        }

        public IActionResult ExportServiceStatusList()
        {
            if (!CheckLoginUser())
                return RedirectToAction("Login", "Account");

            var ServiceStatusList = _db.ServiceStatus.OrderBy(l => l.StatusCode).ToList();

            var columnHeader = new List<string>
            {
                "Status Code",
                "Status Name"
            };

            var fileContent = ExportExcelForServiceStatusList(ServiceStatusList, columnHeader, "ServiceStatus List");
            return File(fileContent, "application/ms-excel", "ServiceStatusList.xlsx");
        }

        public byte[] ExportExcelForServiceStatusList(List<ServiceStatus> ServiceStatusList, List<string> columnHeader, string heading)
        {
            byte[] result = null;
            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(heading);
                worksheet.DefaultColWidth = 20;
                using (var cells = worksheet.Cells[1, 1, 1, 8])
                {
                    cells.Style.Font.Bold = true;
                }

                for (int i = 0; i < columnHeader.Count(); i++)
                {
                    worksheet.Cells[1, i + 1].Value = columnHeader[i];
                }
                var j = 2;
                var count = 1;
                foreach (var item in ServiceStatusList)
                {
                    worksheet.Cells["A" + j].Value = item.StatusCode;
                    worksheet.Cells["B" + j].Value = item.StatusName;
                    j++;
                    count++;
                }
                result = package.GetAsByteArray();
            }
            return result;
        }

        public IActionResult ImportServiceStatusList()
        {
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public IActionResult ImportServiceStatusDataList(IFormFile reportfile)
        {
            session = HttpContext.Session;
            if (!CheckLoginUser())
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                try
                {
                    string folderName = "ImportedFiles";
                    string webRootPath = _env.WebRootPath;
                    string newPath = Path.Combine(webRootPath, folderName);
                    // Delete Files from Directory
                    System.IO.DirectoryInfo di = new DirectoryInfo(newPath);
                    foreach (FileInfo filesDelete in di.GetFiles())
                    {
                        filesDelete.Delete();
                    }// End Deleting files form directories

                    if (!Directory.Exists(newPath))// Crate New Directory if not exist as per the path
                    {
                        Directory.CreateDirectory(newPath);
                    }
                    var fiName = Guid.NewGuid().ToString() + Path.GetExtension(reportfile.FileName);
                    using (var fileStream = new FileStream(Path.Combine(newPath, fiName), FileMode.Create))
                    {
                        reportfile.CopyTo(fileStream);
                    }
                    // Get uploaded file path with root
                    string rootFolder = _env.WebRootPath;
                    string fileName = @"ImportedFiles/" + fiName;
                    FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

                    using (ExcelPackage package = new ExcelPackage(file))
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
                        int totalRows = workSheet.Dimension.Rows;
                        List<ServiceStatus> ServiceStatusList = new List<ServiceStatus>();
                        for (int i = 2; i <= totalRows; i++)
                        {
                            ServiceStatus serviceStatus = new ServiceStatus();
                            serviceStatus.Id = Guid.NewGuid();
                            serviceStatus.StatusCode = workSheet.Cells[i, 1].Value.ToString();
                            serviceStatus.StatusName = workSheet.Cells[i, 2].Value.ToString();

                            serviceStatus.CreatedBy = HttpContext.Session.GetString("userId");
                            serviceStatus.CreatedDate = DateTime.Now;
                            serviceStatus.ModifiedBy = HttpContext.Session.GetString("userId");
                            serviceStatus.ModifiedDate = DateTime.Now;
                            ServiceStatusList.Add(serviceStatus);
                        }

                        _db.ServiceStatus.AddRange(ServiceStatusList);
                        _db.SaveChanges();
                        TempData["Message"] = "Import Data Successfully.";
                        return RedirectToAction("ServiceStatus");
                    }
                }
                catch (Exception ex)
                {
                    TempData["Message"] = ex.Message.ToString();
                    return RedirectToAction("ServiceStatus");
                }
            }

        }
        
        #endregion
    }
}
