﻿using AMI.AccessLog.Project.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Data_Access
{
    public class DataService
    {
        public IConfiguration Configuration { get; set; }

        private SqlConnection connection = null;

        public DataService()
        {
            var builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            connection = new SqlConnection(Configuration.GetConnectionString("DefaultConnection"));
            CheckConnectionState();
        }

        public int AccessLogCase(RequestByFilter model)
        {
            int RowCount = 0;
            try
            {
                string query = "";

                query = "select COUNT(Id) from Register where 1 = 1";
                if (model.fromdate != null && model.todate != null && model.fromdate <= model.todate)
                {
                    query += " AND Convert(date,Date) >= '" + model.fromdate + "' AND Convert(date,Date) <='" + model.todate + "'";
                }
                
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                cmd.CommandText = query;

                RowCount = (int)cmd.ExecuteScalar();

                connection.Close();

                return RowCount;
            }
            catch (Exception)
            {
                return RowCount;
            }
        }

        public int ActivityLogCase(RequestByFilter model)
        {
            int RowCount = 0;
            try
            {
                string query = "";

                query = "select COUNT(Id) from DailyActivity where 1 = 1";
                if (model.fromdate != null && model.todate != null && model.fromdate <= model.todate)
                {
                    query += " AND Convert(date,CreatedDate) >= '" + model.fromdate + "' AND Convert(date,CreatedDate) <='" + model.todate + "'";
                }

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                cmd.CommandText = query;

                RowCount = (int)cmd.ExecuteScalar();

                connection.Close();

                return RowCount;
            }
            catch (Exception)
            {
                return RowCount;
            }
        }

        public DataSet AccessLogList(RequestByFilter model)
        {
            DataSet ds = new DataSet();
            try
            {
                string query = "select t1.Id,Date,t2.Name AS Device,t2.IP,Reason,t3.Name,TechnicianReport,t1.Remark from Register t1" +
                               " inner join Device t2"+
                               " on t1.DeviceId = t2.Id"+
                               " inner join SysUser t3"+
                               " on t3.Id = t1.SysUserId";
                if (model.fromdate != null && model.todate != null && model.fromdate <= model.todate)
                {
                    query += " AND Convert(date,t1.Date) >= '" + model.fromdate + "' AND Convert(date,t1.Date) <='" + model.todate + "'";
                }
                query += " Order By Date Desc";
                
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }
                return ds;

            }
            catch(Exception)
            {
                return ds;
            }
        }

        public DataSet ActivityLogList(RequestByFilter model)
        {
            DataSet ds = new DataSet();
            try
            {
                string query = "select t1.Id,t2.Name,t1.FinishedTask,t1.ContinueTask,t1.CreatedDate from DailyActivity t1" +
                               " inner join SysUser t2" +
                               " ON t1.UserId = t2.Id";
                if (model.fromdate != null && model.todate != null && model.fromdate <= model.todate)
                {
                    query += " AND Convert(date,t1.CreatedDate) >= '" + model.fromdate + "' AND Convert(date,t1.CreatedDate) <='" + model.todate + "'";
                }

                query += " Order By t1.CreatedDate Desc";
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = connection;
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(ds);
                    }
                }
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
        }

        private void CheckConnectionState()
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
