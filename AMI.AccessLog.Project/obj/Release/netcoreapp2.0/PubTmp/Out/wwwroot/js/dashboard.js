﻿$(document).ready(function () {

    $("#txtfromdate").change(function () {
        var fdate = $('#txtfromdate').val();
        var tdate = $('#txttodate').val();
        var request = {};
        request.fromdate = fdate;
        request.todate = tdate;
       
        $.ajax({
            type: "POST", 
            url: "/Home/AccessLogByDate",
            data: JSON.stringify(request),
            contentType: "application/json; charset=utf-8",
            dataType: "Json",
            success: function (response) {
                if (response !== null) {
                    $('#AL').hide();
                    //Change the value of display for
                    $('#divAL').html('<h3>' + response.accessLog + '</h3> <p>Access Log</p>');
                    $('#AT').hide();
                    //Change the value of display for
                    $('#divAT').html('<h3>' + response.activityLog + '</h3> <p>Activity Log</p>');
                }
            }
        });
    });

    $("#txttodate").change(function () {
        var fdate = $('#txtfromdate').val();
        var tdate = $('#txttodate').val();
      
          var request = {};
            request.fromdate = fdate;
            request.todate = tdate;
        
            $.ajax({
                type: "POST",
                url: "/Home/AccessLogByDate",
                data: JSON.stringify(request),
                contentType: "application/json; charset=utf-8",
                dataType: "Json",
                success: function (response) {
                    if (response !== null) {
                        $('#AL').hide();
                        //Change the value of display for
                        $('#divAL').html('<h3>' + response.accessLog + '</h3> <p>Access Log</p>');
                        $('#AT').hide();
                        //Change the value of display for
                        $('#divAT').html('<h3>' + response.activityLog + '</h3> <p>Activity Log</p>');
                }
                }
            });
    });

    
    $.getJSON("/Home/GetTotalProjectCount/", function (data) {
        $("#txtTotalCount").html(data);
    });

    $.getJSON("/Home/GetCompletedProjectCount/", function (data) {
        $("#txtCProjectCount").html(data);
    });

    $.getJSON("/Home/GetOngoingProjectCount/", function (data) {
        $("#txtOProjectCount").html(data);
    });

    $.getJSON("/Home/GetPjStatusNullProjectCount/", function (data) {
        $("#txtNullProjectCount").html(data);
    });

});