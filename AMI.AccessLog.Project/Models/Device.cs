﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class Device
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Ip { get; set; }
        public string Link { get; set; }
        public Guid? LocationId { get; set; }
        public string Remark { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
