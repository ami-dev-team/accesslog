﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class DeviceViewModel
	{
        public Guid Id { get; set; }
        public string DeviceName { get; set; }
        public string Ip { get; set; }
        public string Link { get; set; }
		public string LocationName { get; set; }
        public string Remark { get; set; }
    }
}
