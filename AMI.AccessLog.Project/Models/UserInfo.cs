﻿using System;

namespace AMI.AccessLog.Project.Models
{
    public class UserInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Team { get; set; }
        public string Role { get; set; }
        public string Position { get; set; }
		public bool IsActive { get; set; }
    }
}
