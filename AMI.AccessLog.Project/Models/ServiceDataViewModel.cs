﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Models
{
    public class ServiceDataViewModel
    {
        public Guid Id { get; set; }
        public DateTime? ServiceDate { get; set; }
        public string UserName { get; set; }
        public Guid BranchId { get; set; }
        public Guid DepartmentId { get; set; }
        public string ReasonofRequest { get; set; }
        public Guid ChannelId { get; set; }
        public string Solution { get; set; }
        public Guid ServicePersonId { get; set; }
        public Guid IssueType { get; set; }
        public string Ticket { get; set; }
        public Guid Status { get; set; }
        public string Remark { get; set; }

        public string IssueTypeName { get; set; }
        public string StatusName { get; set; }
        public string BranchName { get; set; }
        public string DepartmentName { get; set; }
        public string ChannelName { get; set; }
        public string ServicePersonName { get; set; }

    }
}
