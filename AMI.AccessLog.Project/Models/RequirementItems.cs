﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class RequirementItems
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
        public Guid ProjectId { get; set; }
        public Guid? StatusId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Guid ImplementTechnicianId { get; set; }
        public Guid CheckTechnicianId { get; set; }
        public string Remark { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
