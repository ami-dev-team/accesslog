﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Models
{
    public class RequestByFilter
    {
        public DateTime? fromdate { get; set; }
        public DateTime? todate { get; set; }
        
    }
}
