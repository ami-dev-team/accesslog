﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class RequirementItemsViewModel
	{
        public Guid Id { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
        public Guid ProjectId { get; set; }
        public string ProjectName { get; set; }
		public Guid StatusId { get; set; }
		public string StatusName { get; set; }
		public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
		public DateTime RStartDate { get; set; }
		public DateTime REndDate { get; set; }
		public Guid ImplementTechnicianId { get; set; }
		public string ImplementTechnicianName { get; set; }
		public Guid CheckTechnicianId { get; set; }
		public string CheckTechnicianName { get; set; }
		public string Remark { get; set; }
    }
}
