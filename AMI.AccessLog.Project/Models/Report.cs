﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Models
{
	public class Report
	{
		public Guid userId { get; set; }
		public Guid TeamId { get; set; }
		public DateTime fromDate { get; set; }
		public DateTime toDate { get; set; }
	}
}
