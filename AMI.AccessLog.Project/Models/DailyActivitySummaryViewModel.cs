﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Models
{
    public class DailyActivitySummaryViewModel
    {
        public Guid Id { get; set; }
        public string FinishedTask { get; set; }
        public string ContinueTask { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
    }
}
