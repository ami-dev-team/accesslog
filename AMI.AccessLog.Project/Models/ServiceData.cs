﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class ServiceData
    {
        public Guid Id { get; set; }
        public DateTime? ServiceDate { get; set; }
        public Guid BranchId { get; set; }
        public Guid DepartmentId { get; set; }
        public string ReasonofRequest { get; set; }
        public Guid ChannelId { get; set; }
        public string Solution { get; set; }
        public Guid ServicePersonId { get; set; }
        public string Ticket { get; set; }
        public string Remark { get; set; }
        public string UserName { get; set; }
        public Guid? IssueType { get; set; }
        public Guid? Status { get; set; }
    }
}
