﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Models
{
    public class UserDetailModel
    {
        public Guid id { get; set; }
        public Register Display { get; set; }
        public Position Role { get; set; }
    }
}
