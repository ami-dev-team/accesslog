﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class DailyActivityReport
	{
		public List<DailyActivityVM> DailyActivityList { get; set; }
		public DateTime fromDate { get; set; }
		public DateTime toDate { get; set; }
		public string TeamName { get; set; }
	}
}
