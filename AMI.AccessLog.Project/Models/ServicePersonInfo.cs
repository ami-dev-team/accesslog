﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Models
{
    public class ServicePersonInfo
    {
        public Guid Id { get; set; }
        public string PersonId { get; set; }
        public string PersonName { get; set; }
        public Guid Position { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string PositionName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}
