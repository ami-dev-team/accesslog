﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class Register
    {
        public Guid Id { get; set; }
        public DateTime? Date { get; set; }
        public Guid DeviceId { get; set; }
        public string Reason { get; set; }
        public Guid SysUserId { get; set; }
        public string TechnicianReport { get; set; }
        public string Remark { get; set; }
        public string TechnicianName { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
