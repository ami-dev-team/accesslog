﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class DailyActivity
    {
        public Guid Id { get; set; }
        public string FinishedTask { get; set; }
        public string ContinueTask { get; set; }
        public DateTime? CreatedDate { get; set; }
        public Guid UserId { get; set; }
    }
}
