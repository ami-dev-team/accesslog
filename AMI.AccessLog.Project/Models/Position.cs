﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class Position
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
