﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Models
{
    public class Dashboard
    {
        public int AccessLog { get; set; }
        public int ActivityLog { get; set; }
    }
}
