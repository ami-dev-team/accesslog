﻿using System;
using System.Collections.Generic;

namespace AMI.AccessLog.Project.Models
{
    public partial class DailyActivityVM
    {
        public Guid Id { get; set; }
        public string FinishedTask { get; set; }
        public string ContinueTask { get; set; }
        public DateTime? CreatedDate { get; set; }
		public DateTime fromDate { get; set; }
		public DateTime toDate { get; set; }
		public Guid UserId { get; set; }
		public string UserName { get; set; }
		public string TeamName { get; set; }
		public Guid TeamId { get; set; }
		public List<DailyActivity> DailyActivityList { get; set; }
        public List<DailyActivitySummaryViewModel> DailyActivitySummaryList { get; set; }
    }
}
