﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Models
{
    public class RegisterViewModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
		public DateTime? Date { get; set; }
		public DateTime? CreatedDate { get; set; }
        public string Device { get; set; }
        public string Ip { get; set; }
        public string Reason { get; set; }
        public string TechnicianName { get; set; }
        public string TechnicianReport { get; set; }
        public string Location { get; set; }
        public string Remark { get; set; }

		public List<RegisterViewModel> AccessLogList { get; set; }
		public DateTime fromDate { get; set; }
		public DateTime toDate { get; set; }
		public string TeamName { get; set; }
		public string StaffName { get; set; }


	}
}
