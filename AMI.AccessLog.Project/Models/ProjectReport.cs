﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AMI.AccessLog.Project.Models
{
	public class ProjectReport
	{
		//public Guid userId { get; set; }
		public DateTime fromDate { get; set; }
		public DateTime toDate { get; set; }
		public DateTime? ProjectDate { get; set; }
		public DateTime? ProjectDateForReport { get; set; }
		public Guid RequirementId { get; set; }		
		public string ProjectName { get; set; }
		public string RequirementDescription { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public DateTime Start { get; set; }
		public DateTime End { get; set; }
		public string StatusName { get; set; }
		public string ImplementTechnicianName { get; set; }
		public string CheckTechnicianName { get; set; }
		public string Remark { get; set; }
		public Guid ProjectId { get; set; }

		public List<ProjectReport> ProjectReportList { get; set; }

	}
}
